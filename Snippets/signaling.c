#include <setjmp.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>

sigjmp_buf env;
timer_t gTimerid;

/* Macro for saving the context */
#define WHILE(cond) while((cond)) { \
    if (sigsetjmp (env, 1) != 0)    \
        printf("Returning\n");      \
    else                            \
        printf("Saved\n");        
#define END_WHILE }

/* Timer logic */
#define PERIOD 500000 /* us */

static void timer_handler ();

static void init( ) __attribute__((constructor));
void init(){
  __attribute__((__unused__)) ssize_t ret;
  struct itimerspec value;

  /* Launch callback */
  (void) signal (SIGALRM, timer_handler);

  /* Start delay */
  value.it_value.tv_sec = 1;
  value.it_value.tv_nsec = 0; /* ns */
  /* Interval delay */
  value.it_interval.tv_sec = 1;
  value.it_interval.tv_nsec = 0; /* ns */

  /* Create and setup the time */
  timer_create (CLOCK_REALTIME, NULL, &gTimerid);
  timer_settime (gTimerid, 0, &value, NULL);

  ret = write(1,"All set\n", 8);
}

static void finish( ) __attribute__((destructor));
void finish(){
  __attribute__((__unused__)) ssize_t ret;
  struct itimerspec value;

  value.it_value.tv_sec = 0;
  value.it_value.tv_nsec = 0;

  value.it_interval.tv_sec = 0;
  value.it_interval.tv_nsec = 0;

  timer_settime (gTimerid, 0, &value, NULL);

  ret = write(1,"Finished\n", 9);
}

/* Interruption */
void timer_handler(int sig){
  __attribute__((__unused__)) ssize_t ret;
  ret = write(1,"Handling timer and returning\n", 29);
  siglongjmp (env, 1);
}

int
main (void)
{
  const int max_counter = 100;
  int counter = 0;

  WHILE(max_counter > counter)
  {
    usleep(100000);
    printf("Current count: %i\n", counter);
    ++counter;
  }
  END_WHILE

  return 0;
}
