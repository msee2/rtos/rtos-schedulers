/**
 * Electronics Engineering Master's
 * Real Time Operating Systems
 * Author: Luis G. Leon Vega <luis@luisleon.me>
 * Copyright 2021 - MIT
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)

typedef struct _TaylorSettings
{
  /* Accumulator */
  double accumulator;
  /* Series configuration */
  uint current_term;
} TaylorSettings;


inline static double
taylor_kernel_fract (const uint term)
{
  double val = 1;

  for (uint i = 1; i <= term; ++i) {
    double num = 2 * i - 1;
    double denum = 2 * i;
    val *= (num / denum);
  }

  return val;
}

/* https://proofwiki.org/wiki/Power_Series_Expansion_for_Real_Arcsine_Function arcsin*/
inline static double
taylor_kernel_compute_term (TaylorSettings * series)
{
  uint term = 0;
  double power = 1;

  double value = 1;

  if (unlikely (!series))
    return -1;

  /* Determine term index */
  term = series->current_term;
  power = 2 * term + 1;

  value = taylor_kernel_fract (term);
  value /= power;

  return value;
}

double
taylor_kernel (const int job_units)
{
  const int num_its = 50;       /* 50 is the minimum */
  TaylorSettings settings = { 0 };

  /* Preserve this order for better caching */
  for (int junit = 0; junit < job_units; ++junit) {
    int offset = junit * num_its;
    for (int it = 0; it < num_its; ++it) {
      settings.current_term = offset + it;
      settings.accumulator += (2 * taylor_kernel_compute_term (&settings));
      printf ("Current val: %f\n", settings.accumulator);
    }
  }

  return settings.accumulator;
}


int
main (void)
{
  double val;
  val = taylor_kernel (5000);
  printf ("Final res: %f\n", val);
  return 0;
}
