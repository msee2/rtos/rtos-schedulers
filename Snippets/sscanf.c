/**
 * Electronics Engineering Master's
 * Real Time Operating Systems
 * Author: Luis G. Leon Vega <luis@luisleon.me>
 * Copyright 2021 - MIT
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char ** argv) {
  char x[50] = "122 324 654";
  int i1 = 0;
  char * token = NULL;
  int * irr = (int *)calloc(3, sizeof(int));
  int * irr_aux = irr;

  token = strtok(x, " ");
  while (token) {
    sscanf(token, "%i", irr_aux++);
    token = strtok(NULL, " ");
  } 
  
  irr_aux = irr;
  for (i1 = 0; i1 < 3; ++i1) {
    fprintf(stdout, "%i\n", *(irr_aux++));
  }

  free(irr);
  return 0;
}
