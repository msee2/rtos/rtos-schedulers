#include <stdio.h>
#include <string.h>

#define FILE_SIZE 1000
#define FIELD_SIZE 14

#define ENABLE_RMT 0b1
#define ENABLE_EDF 0b10
#define ENABLE_LLF 0b100

int result_rm_test (const float mu, const float u);
int result_edf_test (const float mu);
int result_llf_test (const float mu);

int
replace_val (char *content_buffer, const char *pattern, const float val)
{
  char *field = NULL;
  char result[FIELD_SIZE] = "";
  char *subpart = NULL;

  field = strstr (content_buffer, pattern);
  if (!field) {
    return -1;
  }
  subpart = field + FIELD_SIZE;

  memset (result, 0, FIELD_SIZE);
  sprintf (result, "%3.5f\n", val);
  strncpy (field, result, FIELD_SIZE);
  strcat (content_buffer, subpart);

  return 0;
}

int
replace_str (char *content_buffer, const char *pattern, const char *val)
{
  char *field = NULL;
  char result[FIELD_SIZE] = "";
  char *subpart = NULL;

  field = strstr (content_buffer, pattern);
  if (!field) {
    return -1;
  }
  subpart = field + FIELD_SIZE;

  memset (result, 0, FIELD_SIZE);
  strncpy (field, val, FIELD_SIZE);
  strcat (content_buffer, subpart);

  return 0;
}

int
load_template (char *content_buffer, const char *filename)
{
  FILE *template_file = NULL;
  int err = 0;

  template_file = fopen (filename, "r");
  printf ("Loading file: %s\n", filename);
  if (!template_file) {
    perror ("Error while loading the template\n");
    return -1;
  }

  err = fread ((void *) content_buffer, FILE_SIZE, 1, template_file);
  if (err < 0) {
    perror ("Cannot read template\n");
    err = -1;
    goto close;
  }

close:
  fclose (template_file);
  return err;
}

int
write_result (const char *filename, const char *content_buffer)
{
  FILE *output_file = NULL;
  int err = 0;

  output_file = fopen (filename, "w");

  err = (int) fwrite (content_buffer, strlen (content_buffer), 1, output_file);

  return err;
}

/*
 * It should edit: UN_RESULT, MU_RESULT, FI_RESULT
 * They have FIELD_SIZE chars each
 */
int
result_rm_test (const float mu, const float u)
{
  char content_buffer[FILE_SIZE] = { 0 };
  int err = 0;

  err |= load_template (content_buffer, "../docs/rm_test.tex.in");
  if (err)
    return err;

  err |= replace_val (content_buffer, "__UN_RESULT__", u);
  err |= replace_val (content_buffer, "__MU_RESULT__", mu);
  err |=
      replace_str (content_buffer, "_SCHED_RESULT_",
      mu <= u ? "schedulable" : "unschedulable");
  if (err)
    return err;

  err |= write_result ("../docs/rm_test.tex", content_buffer);

  return err;
}

/*
 * It should edit: MU_RESULT, FI_RESULT
 * They have FIELD_SIZE chars each
 */
int
result_edf_test (const float mu)
{
  char content_buffer[FILE_SIZE] = { 0 };
  int err = 0;

  err |= load_template (content_buffer, "../docs/edf_test.tex.in");
  if (err)
    return err;

  err |= replace_val (content_buffer, "__MU_RESULT__", mu);
  err |=
      replace_str (content_buffer, "_SCHED_RESULT_",
      mu <= 1 ? "schedulable" : "unschedulable");
  if (err)
    return err;

  err |= write_result ("../docs/edf_test.tex", content_buffer);

  return err;
}

/*
 * It should edit: MU_RESULT, FI_RESULT
 * They have FIELD_SIZE chars each
 */
int
result_llf_test (const float mu)
{
  char content_buffer[FILE_SIZE] = { 0 };
  int err = 0;

  err |= load_template (content_buffer, "../docs/llf_test.tex.in");
  if (err)
    return err;

  err |= replace_val (content_buffer, "__MU_RESULT__", mu);
  err |=
      replace_str (content_buffer, "_SCHED_RESULT_",
      mu <= 1 ? "schedulable" : "unschedulable");
  if (err)
    return err;

  err |= write_result ("../docs/llf_test.tex", content_buffer);

  return err;
}

int
enable_test_slide (const int flags, const char *filename)
{
  char content_buffer[FILE_SIZE] = { 0 };
  char filename_full[100] = "";
  int err = 0;

  strcat (filename_full, filename);
  err |= load_template (content_buffer, strcat (filename_full, ".in"));
  if (err)
    return err;

  err |=
      replace_str (content_buffer, "__ENABLE_RMT__",
      (flags & ENABLE_RMT) ? "" : "%");
  err |=
      replace_str (content_buffer, "__ENABLE_EDF__",
      (flags & ENABLE_EDF) ? "" : "%");
  err |=
      replace_str (content_buffer, "__ENABLE_LLF__",
      (flags & ENABLE_LLF) ? "" : "%");
  err |=
      replace_str (content_buffer, "__ENABLE_WRP__",
      (flags & ENABLE_WRP) ? "" : "%");
  if (err)
    return err;

  err |= write_result (filename, content_buffer);

  return err;
}

int
main ()
{
  int enable_flags = ENABLE_RMT | ENABLE_EDF;
  result_rm_test (0.834, 0.567);
  result_edf_test (0.345);
  enable_test_slide (enable_flags, "../docs/schedulability.tex");

  return 0;
}
