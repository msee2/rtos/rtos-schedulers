#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define EXECUTION_DOC "docs/execution.tex"

/* Creating a new frame */
#define FRAME_BEGIN(x) "\\begin{frame}{" x "}"
#define FRAME_END "\\end{frame}\n"
#define NUMBER_SLICES_ROW 24
#define NUMBER_ROWS 2
#define TOTAL_CHARS 40000
#define BUFFER_SIZE 1000

/* Create a new table */
#define TABLE_BEGIN_PTT "\\begin{table}[]\n\\begin{tabular}{%s}\n\\hline\n"
#define TABLE_END "\\end{tabular}\n\\end{table}\n"
#define COL_SEPARATOR " & "
#define ROW_SEPARATOR " \\\\ \\hline\n"
#define SEPARATORS_MAX_LEN 56

#define COLOUR_RACK "\\cellcolor[HTML]{%s}"

static char COLOURS[8][7] = {
  "FF0000\0",
  "00FF00\0",
  "0000FF\0",
  "FF00FF\0",
  "FFFF00\0",
  "00FFFF\0",
  "FFFFFF\0",
  "000000\0"
};

#define TASK_ID_MISSED 7

void
create_separators (char separators[SEPARATORS_MAX_LEN], const int slice_idx,
    const int num_slices)
{
  int i = 0;
  memset (separators, 0, SEPARATORS_MAX_LEN);
  if (num_slices - slice_idx > 0) {
    strcat (separators, "|l|");
    for (i = 0; i < NUMBER_SLICES_ROW; ++i) {
      strcat (separators, "l");
      strcat (separators, "|");
    }
  }
}

void
create_frame (int *tasks, const int num_tasks, const int num_slices)
{
  int frame_idx = 0, row_idx = 0, task_idx = 0, slice_idx = 0, idx = 0;
  int total_frames = 0, total_rows = 0;
  int task_id = 0, slide_top = 0;

  char *frame = (char *) calloc (TOTAL_CHARS, 1);
  char *buffer = (char *) calloc (BUFFER_SIZE, 1);
  char separators[SEPARATORS_MAX_LEN] = "";

  total_rows = num_slices / NUMBER_SLICES_ROW + 1;
  total_frames = total_rows / NUMBER_ROWS + 1;

  /* Frame loop */
  for (frame_idx = 0; frame_idx < total_frames; ++frame_idx) {
    strcat (frame, FRAME_BEGIN ("Test"));

    /* Row loop */
    for (row_idx = 0; row_idx < NUMBER_ROWS; ++row_idx) {
      /* If all the slices are filled, no more work to do */
      if (idx >= num_slices)
        break;
      sprintf (buffer, "Starting time from: $t = %i$", idx);
      strcat (frame, buffer);
      memset ((void *) buffer, 0, BUFFER_SIZE);
      create_separators (separators, idx, num_slices);
      sprintf (buffer, TABLE_BEGIN_PTT, separators);
      strcat (frame, buffer);

      /* Task loop */
      slide_top = idx;
      for (task_idx = 0; task_idx < num_tasks; ++task_idx) {
        idx = slide_top;
        memset ((void *) buffer, 0, BUFFER_SIZE);
        sprintf (buffer, "Task %i", task_idx + 1);
        strcat (frame, buffer);

        /* Slice loop */
        for (slice_idx = 0; slice_idx < NUMBER_SLICES_ROW; ++slice_idx) {
          if (idx >= num_slices)
            break;
          strcat (frame, COL_SEPARATOR);
          memset ((void *) buffer, 0, BUFFER_SIZE);
          task_id = *(tasks + (idx++));

          sprintf (buffer, COLOUR_RACK, COLOURS[task_id]);
          strcat (frame, (task_id == task_idx
                  || task_id == TASK_ID_MISSED) ? buffer : "");
        }
        strcat (frame, ROW_SEPARATOR);
      }
      idx = slide_top + slice_idx;

      strcat (frame, TABLE_END);
    }
    strcat (frame, FRAME_END);
  }

  printf ("%s", frame);

  free (frame);
  free (buffer);
}

int
main ()
{
  const int n_tasks = 6;
  const int n_slices = 100;
  int i = 0;
  int *slices = (int *) calloc (sizeof (int), n_slices);

  for (i = 0; i < n_slices; ++i) {
    slices[i] = i % n_tasks;
  }

  slices[n_slices / 2] = n_tasks + 1;
  create_frame (slices, n_tasks, n_slices);
  return 0;
}
