#include <gtk/gtk.h>
#include <string.h>
#include <math.h>

#define MAX_TASKS 6
#define MIN_TASKS 1

typedef struct
{
  guint index;
  guint period;
  guint compute;
} TaskConfig;

typedef struct
{
  /* Scheduler selection */
  gboolean rm_scheduler;
  gboolean edf_scheduler;
  gboolean llf_scheduler;

  /* Presentation mode */
  gboolean show_per_slide;
  gboolean animate;
  TaskConfig configs[MAX_TASKS];

  /* Properties */
  guint minimum_multiplier;
  gfloat u_n;
  gfloat processing_load;

  /* Results */
  gboolean rm_schedulable;
  gboolean edf_schedulable;
  gboolean llf_schedulable;
} AnalysisConfig;

typedef struct
{
  GtkWindow *window;
  GtkBuilder *builder;
  guint n_tasks;
  GList *task_rows;
  GMutex lock;
  AnalysisConfig analysis;
} Gui;

/* Signals */
void on_add_new_task (GtkWidget * widget, gpointer * data);
void on_remove_task (GtkWidget * widget, gpointer * data);
void on_start_execution (GtkWidget * widget, gpointer * data);

/* Helpers */
inline gboolean validate_entry (TaskConfig * config);

void
on_window_main_destroy ()
{
  gtk_main_quit ();
}

void
init_task_array (Gui * gui)
{
  GtkWidget *row = NULL;
  int i = MIN_TASKS;
  gchar *row_str = NULL;

  g_return_if_fail (gui);

  g_mutex_lock (&gui->lock);
  for (i = MIN_TASKS; i <= MAX_TASKS; ++i) {
    row_str = g_strdup_printf ("task-row-%i", i);

    row = GTK_WIDGET (gtk_builder_get_object (gui->builder, row_str));
    gui->task_rows = g_list_append (gui->task_rows, row);
    gui->analysis.configs[i - 1].index = i;

    if (row_str) {
      g_free (row_str);
      row_str = NULL;
    }
  }
  g_mutex_unlock (&gui->lock);
}

void
init_buttons (Gui * gui)
{
  GtkWidget *button;

  g_return_if_fail (gui);

  button = GTK_WIDGET (gtk_builder_get_object (gui->builder, "add-task-btn"));
  g_signal_connect (button, "clicked", G_CALLBACK (on_add_new_task),
      (gpointer) (gui));

  button =
      GTK_WIDGET (gtk_builder_get_object (gui->builder, "remove-task-btn"));
  g_signal_connect (button, "clicked", G_CALLBACK (on_remove_task),
      (gpointer) (gui));

  button = GTK_WIDGET (gtk_builder_get_object (gui->builder, "start-btn"));
  g_signal_connect (button, "clicked", G_CALLBACK (on_start_execution),
      (gpointer) (gui));
}

void
on_add_new_task (GtkWidget * widget, gpointer * data)
{
  Gui *gui = NULL;
  GtkWidget *row = NULL;

  g_return_if_fail (data);
  gui = (Gui *) (data);

  g_mutex_lock (&gui->lock);
  gui->n_tasks = gui->n_tasks == MAX_TASKS ? MAX_TASKS : ++gui->n_tasks;
  row = GTK_WIDGET (g_list_nth_data (gui->task_rows, gui->n_tasks - 1));
  gtk_widget_set_visible (row, TRUE);

  g_mutex_unlock (&gui->lock);
}

/* Non-Thread Safe - Execute within a critical zone only with guards */
inline gboolean
validate_entry (TaskConfig * config)
{
  g_return_val_if_fail (config, FALSE);

  return config->period >= config->compute;
}

void
on_remove_task (GtkWidget * widget, gpointer * data)
{
  Gui *gui = NULL;
  GtkWidget *row = NULL;

  g_return_if_fail (data);
  gui = (Gui *) (data);

  g_mutex_lock (&gui->lock);
  gui->n_tasks = gui->n_tasks == MIN_TASKS ? MIN_TASKS : --gui->n_tasks;

  row = GTK_WIDGET (g_list_nth_data (gui->task_rows, gui->n_tasks));
  gtk_widget_set_visible (row, FALSE);

  g_mutex_unlock (&gui->lock);
}

inline void
set_task_config (Gui * gui, TaskConfig * config, guint index)
{
  gchar *ci_field_str = NULL;
  gchar *pi_field_str = NULL;
  GtkWidget *c_field = NULL;
  GtkWidget *p_field = NULL;

  g_return_if_fail (config);
  g_return_if_fail (gui);

  index++;

  ci_field_str = g_strdup_printf ("t%i-c", index);
  pi_field_str = g_strdup_printf ("t%i-p", index);

  c_field = GTK_WIDGET (gtk_builder_get_object (gui->builder, ci_field_str));
  p_field = GTK_WIDGET (gtk_builder_get_object (gui->builder, pi_field_str));

  config->compute = atoi (gtk_entry_get_text (GTK_ENTRY (c_field)));
  config->period = atoi (gtk_entry_get_text (GTK_ENTRY (p_field)));

  g_free (ci_field_str);
  g_free (pi_field_str);
}

inline guint
compute_lcm (AnalysisConfig * analysis_config, guint tasks)
{
  guint lcm = 1;
  guint values[MAX_TASKS];
  gint i = 0;

  g_return_val_if_fail (analysis_config, 1);
  memset ((void *) values, 1, sizeof (guint) * MAX_TASKS);

  /* Load */
  for (i = 0; i < tasks; ++i) {
    values[i] = analysis_config->configs[i].period;
  }

  /* Compute */
  for (i = 0; i < tasks; ++i) {
    // Todo
  }

  return lcm;
}

inline gfloat
compute_load (AnalysisConfig * analysis_config, guint tasks)
{
  gfloat load = 0;
  gint i = 0;

  g_return_val_if_fail (analysis_config, 1);

  /* Load */
  for (i = 0; i < tasks; ++i) {
    load +=
        (gfloat) (analysis_config->configs[i].compute) /
        (gfloat) (analysis_config->configs[i].period);
  }

  return load;
}

inline gfloat
compute_un (AnalysisConfig * analysis_config, guint tasks)
{
  gfloat un = 0;

  g_return_val_if_fail (analysis_config, 1);

  un = -1;
  un += pow (2, 1.f / tasks);
  un *= (gfloat) (tasks);

  return un;
}

inline void
show_common_values (Gui * gui)
{
  GtkWidget *label;
  gchar *val_str;

  g_return_if_fail (gui);

  /* Get the LCM */
  label = GTK_WIDGET (gtk_builder_get_object (gui->builder, "mmc-lbl"));
  val_str = g_strdup_printf ("%u", gui->analysis.minimum_multiplier);
  gtk_label_set_text (GTK_LABEL (label), val_str);
  g_free (val_str);

  label = GTK_WIDGET (gtk_builder_get_object (gui->builder, "u-lbl"));
  val_str = g_strdup_printf ("%3.5f", gui->analysis.u_n);
  gtk_label_set_text (GTK_LABEL (label), val_str);
  g_free (val_str);

  label = GTK_WIDGET (gtk_builder_get_object (gui->builder, "miu-lbl"));
  val_str = g_strdup_printf ("%3.5f", gui->analysis.processing_load);
  gtk_label_set_text (GTK_LABEL (label), val_str);
  g_free (val_str);
}

void
compute_values (Gui * gui)
{
  AnalysisConfig *analysis_config = NULL;

  g_return_if_fail (gui);
  analysis_config = &(gui->analysis);

  /* Compute Common Values */
  analysis_config->minimum_multiplier =
      compute_lcm (analysis_config, gui->n_tasks);
  analysis_config->u_n = compute_un (analysis_config, gui->n_tasks);
  analysis_config->processing_load =
      compute_load (analysis_config, gui->n_tasks);

  /* Show */
  show_common_values (gui);
}

void
get_selected_options (Gui * gui)
{
  AnalysisConfig *analysis_config = NULL;
  GtkWidget *toggle = NULL;
  GtkWidget *label = NULL;
  gchar *val_str = NULL;

  g_return_if_fail (gui);
  analysis_config = &(gui->analysis);

  toggle = GTK_WIDGET (gtk_builder_get_object (gui->builder, "rm-chk"));
  analysis_config->rm_scheduler =
      gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle));
  toggle = GTK_WIDGET (gtk_builder_get_object (gui->builder, "edf-chk"));
  analysis_config->edf_scheduler =
      gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle));
  toggle = GTK_WIDGET (gtk_builder_get_object (gui->builder, "llf-chk"));
  analysis_config->llf_scheduler =
      gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle));

  /* Schedulers */
  label = GTK_WIDGET (gtk_builder_get_object (gui->builder, "schd-lbl"));
  val_str = g_strdup_printf ("%s%s%s",
      analysis_config->rm_scheduler ? "RM" : "",
      analysis_config->edf_scheduler ? " EDF" : "",
      analysis_config->llf_scheduler ? " LLF" : "");
  gtk_label_set_text (GTK_LABEL (label), val_str);

  /* Slide options */
  toggle =
      GTK_WIDGET (gtk_builder_get_object (gui->builder, "singleslide-chk"));
  analysis_config->show_per_slide =
      gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle));
  toggle = GTK_WIDGET (gtk_builder_get_object (gui->builder, "animated-chk"));
  analysis_config->animate =
      gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle));
}

void
on_start_execution (GtkWidget * widget, gpointer * data)
{
  GtkDialogFlags flags = GTK_DIALOG_DESTROY_WITH_PARENT;

  Gui *gui = NULL;
  AnalysisConfig *analysis_config;
  TaskConfig *task_config;
  gboolean valid = TRUE;
  GtkWidget *dialog;
  GtkWidget *button;
  gint i = 0;

  g_return_if_fail (data);
  gui = (Gui *) (data);

  analysis_config = &(gui->analysis);

  button = GTK_WIDGET (gtk_builder_get_object (gui->builder, "start-btn"));
  gtk_widget_set_sensitive (button, FALSE);

  /* Get the task info for every defined task */
  for (i = 0; i < gui->n_tasks; ++i) {
    /* Get task info */
    task_config = &(analysis_config->configs[i]);
    set_task_config (gui, task_config, i);
    valid = validate_entry (task_config);
    if (!valid) {
      dialog = gtk_message_dialog_new (gui->window,
          flags,
          GTK_MESSAGE_ERROR,
          GTK_BUTTONS_CLOSE,
          "Error while defining task '%i'. Please check Pi and Ci", i + 1);
      gtk_dialog_run (GTK_DIALOG (dialog));
      gtk_widget_destroy (dialog);
      goto enable_button;
    }
  }

  /* Get scheduler selection */
  get_selected_options (gui);

  /* Compute values */
  compute_values (gui);

enable_button:
  gtk_widget_set_sensitive (button, TRUE);
}

int
main (int argc, char *argv[])
{
  GtkBuilder *builder;
  GtkWidget *window;
  Gui gui;

  gui.n_tasks = 1;
  gui.task_rows = NULL;
  memset ((void *) &(gui.analysis), 0, sizeof (AnalysisConfig));

  gtk_init (&argc, &argv);

  builder = gtk_builder_new ();
  gtk_builder_add_from_file (builder, "gui/gui.glade", NULL);
  gui.builder = builder;
  g_mutex_init (&(gui.lock));


  /* Connect the callback for the Windows */
  window = GTK_WIDGET (gtk_builder_get_object (builder, "window"));
  g_signal_connect (window, "destroy", G_CALLBACK (on_window_main_destroy),
      NULL);
  gui.window = GTK_WINDOW (window);

  /* Initialise the tasks */
  init_task_array (&gui);
  init_buttons (&gui);

  /* Show the window */
  gtk_widget_show (window);
  gtk_main ();

  /* Unreference the dynamic objects */
  g_object_unref (builder);
  g_mutex_clear (&(gui.lock));
  g_list_free (gui.task_rows);

  return 0;
}
