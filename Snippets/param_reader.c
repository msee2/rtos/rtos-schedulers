#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * SchedConfigs:
 * @property scheduler Scheduling algo: 0 = lottery, 1 = RR
 * @property configs Parameters for the scheduler
 * @property mode Operation mode: 0 = non-preemptive, 1 = preemptive
 * @property n_procs Number of processes
 * @property arr_times Arrival time
 * @property j_load Job load
 * @property tickets Parameters per job (lottery tickets)
 * @property quantum Completed job for a yield or quantum
 */
typedef struct {
  int scheduler;
  int configs[10];
  int mode;
  int n_procs;
  long * arr_times;
  int * j_load;
  int * tickets;
  int quantum;
} SchedConfigs;

/**
 * sched_configs_get_int_array:
 * Gets an int array from a line
 * @param out output array (must be allocated by the user)
 * @param in input line
 * @param len length allocated by the array
 */
void sched_configs_get_int_array (int * out, char * in, size_t len) {
  char * token = NULL;
  int i = 0;
  int * cur_pos = out;

  if (!out) return;

  token = strtok(in, " ");
  while (token && (i++) < len) {
    sscanf(token, "%i", cur_pos++);
    token = strtok(NULL, " ");
  }
}

/**
 * sched_configs_get_long_array:
 * Gets an int array from a line
 * @param out output array (must be allocated by the user)
 * @param in input line
 * @param len length allocated by the array
 */
void sched_configs_get_long_array (long * out, char * in, size_t len) {
  char * token = NULL;
  int i = 0;
  long * cur_pos = out;

  if (!out) return;

  token = strtok(in, " ");
  while (token && (i++) < len) {
    sscanf(token, "%li", cur_pos++);
    token = strtok(NULL, " ");
  }
}

/**
 * sched_configs_new:
 * Allocates a new SchedConfigs struct
 */
SchedConfigs * sched_configs_new () {
  return (SchedConfigs *)calloc(1, sizeof(SchedConfigs));
}

/**
 * sched_configs_free:
 * Frees the struct
 */
void sched_configs_free (SchedConfigs * s) {
  if (!s) return;
  free(s);
}

/**
 * sched_configs_print:
 * Prints the configurations from configs
 * @param configs configs to print in console
 */
void sched_configs_print (SchedConfigs * configs) {
  if (!configs) return;

  fprintf(stdout, "Scheduler: %s\n", configs->scheduler ? "RR" : "LOTTERY");
  fprintf(stdout, "Mode: %s\n", configs->mode ? "Preemptive" : "Non-preemptive");
  fprintf(stdout, "Procs: %i\n", configs->n_procs);
  fprintf(stdout, "Arrival Times - Job Load - Tickets:\n");
  for (int i = 0; i < configs->n_procs; ++i) {
    fprintf(stdout, "%li - %i - %i\n",
      configs->arr_times[i],
      configs->j_load[i],
      configs->tickets[i]
    );
  }
}

/**
 * sched_configs_read:
 * Reads the configs from the file
 * @param configs configs to load in (must be allocated by the user)
 * @param filename string with the filename
 */
bool sched_configs_read (SchedConfigs * configs, const char * filename) {
  size_t len;
  char * line = NULL;
  ssize_t read = 0;

  if (!configs || !filename) return false;
  
  FILE *file = fopen(filename, "r");
  if (!file) {
    fprintf(stderr, "Unable to open the file %s.\n", filename);
    return false;
  }

  int cnt = 0;

  while((read = getline(&line, &len, file)) != -1) {
    switch (cnt) {
      case 0:
        sscanf(line, "%i", &configs->scheduler);
        break;
      case 1:
        break;
      case 2:
        sscanf(line, "%i", &configs->mode);
        break;
      case 3:
        sscanf(line, "%i", &configs->n_procs);
        if (configs->n_procs >= 5 && configs->n_procs <= 25) {
          configs->arr_times = (long *)calloc(sizeof(long), configs->n_procs);
          configs->j_load = (int *)calloc(sizeof(int), configs->n_procs);
          configs->tickets = (int *)calloc(sizeof(int), configs->n_procs);
        }
        else {
          fprintf(stderr, "Invalid number of procs\n");
          return false;
        }
        break;
      case 4:
        sched_configs_get_long_array(
          configs->arr_times,
          line,
          configs->n_procs);
        break;
      case 5:
        sched_configs_get_int_array(
          configs->j_load,
          line,
          configs->n_procs);
        break;
      case 6:
        sched_configs_get_int_array(
          configs->tickets,
          line,
          configs->n_procs);
        break;
      case 7:
        sscanf(line, "%i", &configs->quantum);
        break;
    }
    ++cnt;
  }
  fclose(file);
  if (line) free (line);
  return true;
}

int main(int argc, char ** argv) {
  SchedConfigs configs = {0};

  __attribute__((__unused__)) int ret = 0;

  if (argc < 2) {
    fprintf(stderr, "Error loading the file.\n"
                    "Usage:\n"
                    "%s <FILE.sched>\n", argv[0]);
    return -1;
  }

  sched_configs_read(&configs, argv[1]);

  sched_configs_print(&configs);
  return 0;
}
