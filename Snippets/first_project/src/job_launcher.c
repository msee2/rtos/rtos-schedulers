/**
 * Electronics Engineering Master's
 * Real Time Operating Systems
 * Author: Luis G. Leon Vega <luis@luisleon.me>
 * Copyright 2021 - MIT
 */

#include <stdio.h>
#include <string.h>

#include "job_launcher.h"
#include "list.h"
#include "schedulers.h"
#include "timer.h"

static List *waiting_queue_ = NULL;
static schedule_t scheduler_caller_ = NULL;
static void *scheduler_ = NULL;

static void
job_deploy (__sigval_t sigval)
{
  TimerSched *tm;
  Node *waiting_node;

  if (!sigval.sival_ptr)
    return;
  tm = (TimerSched *) sigval.sival_ptr;
  waiting_node = tm->node;

  fprintf (stdout, "[ %lli ] Moving: %i\n", timer_cur_time (), tm->id);

  /* Transfer for waiting to ready */
  scheduler_caller_ (scheduler_, tm->task, tm->priority);

  timer_sched_delete ((TimerSched **) & tm);
  list_pop (waiting_queue_, tm->node);
  node_delete (&waiting_node);
}

/**
 * job_launcher:
 * Launches the job at a certain time. The list must be initialised before
 * calling the launcher.
 * `arrival_times` must have the same number of elements as n_procs
 */
void
job_launcher (List * waiting, schedule_t sch_caller, void *sch, Task * tasks,
    int n_procs, long *arrival_times, int *priorities)
{
  Node *cur_node;
  TimerSched *tm;

  if (!waiting)
    return;
  if (!sch_caller || !sch)
    return;
  if (!tasks)
    return;
  if (!arrival_times || !priorities)
    return;

  waiting_queue_ = waiting;
  scheduler_caller_ = sch_caller;
  scheduler_ = sch;

  if (n_procs <= 0)
    return;

  /* Create the timers */
  for (int i = 0; i < n_procs; ++i) {
    long time_ns = arrival_times[i] * 1000000;
    cur_node = node_create ();

    tasks[i].id = i;
    tm = timer_sched_create (job_deploy, &tasks[i], time_ns, 0, i);
    cur_node->data = (void *) tm;
    tm->node = cur_node;
    tm->priority = &priorities[i];
    list_append (waiting, cur_node);
  }

  /* Start the timers: try to emulate a start with respect at the same
     point */
  fprintf (stdout, "[ %lli ] Reference time\n", timer_cur_time ());
  for (cur_node = waiting->head; cur_node != NULL; cur_node = cur_node->next) {
    tm = (TimerSched *) cur_node->data;
    timer_sched_play (tm);
  }
}
