/**
 * Electronics Engineering Master's
 * Real Time Operating Systems
 * Author: Luis G. Leon Vega <luis@luisleon.me>
 * Copyright 2021 - MIT
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "job.h"
#include "taylor.h"

#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)

typedef struct _TaylorSettings
{
  /* Accumulator */
  double accumulator;
  /* Series configuration */
  uint current_term;
  /* Job numer */
  uint job_unit_num;
} TaylorSettings;


inline static double
taylor_kernel_fract (const uint term)
{
  double val = 1;

  for (uint i = 1; i <= term; ++i) {
    double num = 2 * i - 1;
    double denum = 2 * i;
    val *= (num / denum);
  }

  return val;
}

/* https://proofwiki.org/wiki/Power_Series_Expansion_for_Real_Arcsine_Function arcsin*/
inline static double
taylor_kernel_compute_term (TaylorSettings * series)
{
  uint term = 0;
  double power = 1;

  double value = 1;

  if (unlikely (!series))
    return -1;

  /* Determine term index */
  term = series->current_term;
  power = 2 * term + 1;

  value = taylor_kernel_fract (term);
  value /= power;

  return value;
}

double
taylor_kernel (TaylorSettings * settings)
{
  const int num_its = 50;       /* 50 is the minimum */

  /* Preserve this order for better caching */
  int offset = settings->job_unit_num * num_its;
  for (int it = 0; it < num_its; ++it) {
    settings->current_term = offset + it;
    settings->accumulator += (2 * taylor_kernel_compute_term (settings));
  }

  /* Partial result */
  return settings->accumulator;
}

/* Let's assume that info are the arguments of any program we do in
   user space. At the end, this is the impression when using MPI or
   any executer */
bool
taylor (Task * argv)
{
  double val = 0.;
  TaylorSettings settings = { 0 };

  if (!argv)
    return true;

/* *INDENT-OFF* */
  WHILE (1)
    GET_CHECKPOINT(&settings, TaylorSettings)
    if (settings.job_unit_num >= argv->n_jobs) break;

    fprintf (stdout, "Sample: %u out of %i\n", settings.job_unit_num,
      argv->n_jobs);
    val = taylor_kernel (&settings);
    
    fprintf (stdout, "Partial res: %f\n", val);
    SET_PI_VAL(val);

    settings.job_unit_num++;

    SET_CHECKPOINT(&settings, TaylorSettings)
  END_WHILE
/* *INDENT-ON* */
  fprintf (stdout, "Final res: %f\n", settings.accumulator);

  return true;
}
