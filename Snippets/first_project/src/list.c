/**
 * Electronics Engineering Master's
 * Real Time Operating Systems
 * Author: Luis G. Leon Vega <luis@luisleon.me>
 * Copyright 2021 - MIT
 */

#include "list.h"

void
list_append (List * list, Node * elem)
{
  if (!list || !elem)
    return;

  elem->next = NULL;
  elem->prev = NULL;

  if (!list->tail) {
    list->head = elem;
    list->tail = elem;
  } else {
    list->tail->next = elem;
    elem->prev = list->tail;
    list->tail = elem;
  }
  elem->index = list->lind++;
  ++list->elems;
}

void
list_pop (List * list, Node * elem)
{
  if (!elem)
    return;

  if (elem->prev) {
    elem->prev->next = elem->next;
  }
  if (elem->next) {
    elem->next->prev = elem->prev;
  }
  if (elem == list->tail)
    list->tail = elem->prev;
  if (elem == list->head)
    list->head = elem->next;
  --list->elems;
}

Node *
node_create ()
{
  return (Node *) calloc (1, sizeof (Node));
}

void
node_delete (Node ** node)
{
  if (!node)
    return;
  if (!(*node))
    return;

  free (*node);
  *node = NULL;
}
