/**
 * Electronics Engineering Master's
 * Real Time Operating Systems
 * Author: Luis G. Leon Vega <luis@luisleon.me>
 * Copyright 2021 - MIT
 */

#include "gui.h"

#define TITLE_TEMPLATE "Worker: %i - Pi: %f - Jobs: %i"

#if 1
static gboolean
gui_fill (gpointer user_data)
{
  gchar *percentage = NULL;
  gdouble percentage_f = 0.;
  gchar *title_str = NULL;
  GUIApp *gui;
  gdouble fraction;
  ProgressBar *bar = NULL;
  Task *task = NULL;
  GtkWidget *title = NULL;
  GtkWidget *progress_bar = NULL;
  GtkWidget *label = NULL;
  gint cur_bar = 0;
  gint n_procs = 0;

  g_return_val_if_fail (user_data, FALSE);
  gui = (GUIApp *) user_data;
  bar = gui->bars;

  g_return_val_if_fail (bar, FALSE);
  g_return_val_if_fail (gui->configs, FALSE);
  g_return_val_if_fail (gui->tasks, FALSE);
  task = gui->tasks;
  n_procs = gui->configs->n_procs;

  for (cur_bar = 0; cur_bar < n_procs; ++cur_bar) {
    progress_bar = bar[cur_bar].progress_bar;
    label = bar[cur_bar].label;
    title = bar[cur_bar].title;
    g_return_val_if_fail (progress_bar, FALSE);
    g_return_val_if_fail (label, FALSE);
    g_return_val_if_fail (title, FALSE);

    /* Compute fraction */
    fraction = ((gdouble) (task[cur_bar].job_done)) / task[cur_bar].n_jobs;
    percentage_f = (fraction > 1 ? 1 : fraction) * 100;
    percentage = g_strdup_printf (" %i %%  ", (int) (percentage_f));
    gtk_label_set_text (GTK_LABEL (label), percentage);
    gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (progress_bar), fraction);

    /* Update title */
    title_str =
        g_strdup_printf (TITLE_TEMPLATE, cur_bar, task[cur_bar].pi_val,
        task[cur_bar].n_jobs);
    gtk_label_set_text (GTK_LABEL (title), title_str);

    g_free (percentage);
    g_free (title_str);
    percentage = NULL;
    title_str = NULL;
  }

  return TRUE;
}
#endif

static void
gui_activate (GtkApplication * app, gpointer user_data)
{
  GUIApp *gui;
  GtkWidget *window;
  GtkWidget *progress_bar;
  GtkWidget *label, *progress_bar_label;
  GtkWidget *grid;
  gint width = 600;
  gint height = 50;
  gint cols = 2;
  gint rows = 1;
  gint height_per_row = 50;
  gint cur_bar = 0;
  gint n_procs = 0;
  gchar *title = NULL;
  gint bar_rows = 4;
  gint bar_cols = 15;
  gint row_offset = 4;
  ProgressBar *cur_bar_ptr = NULL;

  g_return_if_fail (user_data);

  gui = (GUIApp *) user_data;
  g_return_if_fail (gui->configs);

  /* Compute the size */
  n_procs = gui->configs->n_procs;
  rows = n_procs / cols;
  height = rows * height_per_row;

  /*Create a window with a title, and a default size */
  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "Scheduler Project");
  gtk_window_set_default_size (GTK_WINDOW (window), width, height);

  /* Create a table for layout  */
  grid = gtk_grid_new ();
  gtk_container_add (GTK_CONTAINER (window), grid);

  /* Create the bars */
  gui->bars = g_new0 (ProgressBar, n_procs);
  cur_bar_ptr = gui->bars;


  title =
      g_strdup_printf ("---- Scheduler: %s -----",
      gui->configs->scheduler ? "RR" : "LOTTERY");
  label = gtk_label_new (title);
  g_object_set (label, "margin", 12, NULL);
  gtk_grid_attach (GTK_GRID (grid), label, 0, 0, 30, 2);
  gtk_widget_show (label);
  g_free (title);
  title = NULL;

  for (cur_bar = 0; cur_bar < n_procs; ++cur_bar) {
    gint cur_row = (cur_bar / cols) * bar_rows + row_offset;
    gint cur_col = (cur_bar % cols) * bar_cols;

    /* Title */
    title = g_strdup_printf (TITLE_TEMPLATE, cur_bar, 0.,
        gui->tasks[cur_bar].n_jobs);
    label = gtk_label_new (title);
    gtk_grid_attach (GTK_GRID (grid), label, cur_col, cur_row, 2, 2);
    gtk_widget_show (label);
    cur_bar_ptr[cur_bar].title = label;

    /* Bar */
    progress_bar = gtk_progress_bar_new ();
    gtk_grid_attach (GTK_GRID (grid), progress_bar, cur_col, cur_row + 2, 10,
        2);
    cur_bar_ptr[cur_bar].progress_bar = progress_bar;

    /* Bar label */
    progress_bar_label = gtk_label_new (" 0 %  ");
    gtk_grid_attach_next_to (GTK_GRID (grid), progress_bar_label, progress_bar,
        GTK_POS_RIGHT, 2, 2);
    gtk_widget_show (progress_bar_label);
    cur_bar_ptr[cur_bar].label = progress_bar_label;

    /* Set fraction */
    gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (progress_bar), 0.);

    g_free (title);
    title = NULL;
  }

  /* Use the created fill function every 500 milliseconds */
  g_timeout_add (100, gui_fill, gui);

  gtk_widget_show_all (window);
}

int
gui_run_app (GUIApp * gui)
{
  int ret = 0;
  g_return_val_if_fail (gui, -1);

  g_signal_connect (gui->app, "activate", G_CALLBACK (gui_activate),
      (gpointer) gui);
  ret = g_application_run (G_APPLICATION (gui->app), gui->argc, gui->argv);
  return ret;
}

/**
 * gui_create_app:
 * @param argc number of arguments
 * @param argv arguments
 */
GUIApp *
gui_create_app (int argc, char **argv)
{
  GUIApp *gui = NULL;

  g_return_val_if_fail (argv, NULL);

  gui = g_new0 (GUIApp, 1);

  gui->app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  gui->argc = argc;
  gui->argv = argv;

  return gui;
}

/**
 * gui_free_destroy:
 * @param gui GUI to destroy
 */
void
gui_free_destroy (GUIApp ** gui)
{
  g_return_if_fail (gui);
  g_return_if_fail ((*gui));

  if ((*gui)->app)
    g_object_unref ((*gui)->app);
  if ((*gui)->bars)
    g_free ((*gui)->bars);
  g_free (*gui);
  *gui = NULL;
}
