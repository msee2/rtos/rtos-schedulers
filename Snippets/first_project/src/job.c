/**
 * Electronics Engineering Master's
 * Real Time Operating Systems
 * Author: Luis G. Leon Vega <luis@luisleon.me>
 * Copyright 2021 - MIT
 */

#include <stdbool.h>
#include <stdlib.h>

#include "job.h"

#include "taylor.h"

bool
job_execute (Task * info)
{
  bool ret = true;
  /* Guards */
  if (!info)
    return true;
  if (!info->task)
    return true;

  ret = (info->task) (info);
  if (ret) {
    if (!info->user_data)
      free (info->user_data);
    info->user_data = NULL;
  }
  return ret;
}
