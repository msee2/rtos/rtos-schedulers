/**
 * Electronics Engineering Master's
 * Real Time Operating Systems
 * Author: Luis G. Leon Vega <luis@luisleon.me>
 * Copyright 2021 - MIT
 * Based on Waldspurger PhD Thesis
 */

#include <stdlib.h>
#include <stdio.h>

#include "list.h"
#include "rr.h"

extern void timer_handler (int sig);

void
rr_client_join (RRSched * client, RRClient * c, Task * job)
{
  if (!client || !c)
    return;

  Node *node = node_create ();
  list_append (&client->ready_poll, node);

  c->task = job;
  c->iters = 0;
  c->num = node->index;

  client->num_threads++;
  client->acc_num_threads++;
  fprintf (stdout, "Joining\n");
  node->data = (void *) c;
};

void
rr_client_rejoin (RRSched * client, RRClient * c)
{
  if (!client || !c)
    return;

  Node *node = node_create ();
  list_append (&client->ready_poll, node);

  node->data = (void *) c;
};

void
rr_client_leave (RRSched * client, RRClient * c)
{
  if (!c)
    return;
  if (!client)
    return;

  client->num_threads--;

  if (c)
    free (c);
}

void
rr_allocate (RRSched * client)
{
  RRClient *c = NULL;
  Node *node = NULL;
  List *list = NULL;
  bool ret = true;

  if (!client)
    return;
  list = &client->ready_poll;

  /* Check if there is an unfinished thread */
  if (client->current_client) {
    c = client->current_client;
    if (c->finished)
      goto release;
    rr_client_rejoin (client, c);
  }

  /* Get a new thread */
  node = list->head;
  if (!node)
    return;
  if (!node->data)
    return;
  c = (RRClient *) node->data;
  list_pop (list, node);

  /* Remove from the list and free memory */
  node_delete (&node);
  client->current_client = c;


  /* Act accordingly */
  if (!c->task)
    goto release;

  fprintf (stdout, "Task: %i\n", c->task->id);
  ret = job_execute (c->task);
  c->finished = ret;

  /* Check termination */
  if (ret) {
    goto release;
  }

  return;
release:
  rr_client_leave (client, c);
  client->current_client = NULL;

  /* This is not applicable to a real case scenario, since the
     scheduler will wait for threads or NOP threads */
}

RRClient *
rr_create_client ()
{
  return (RRClient *) calloc (1, sizeof (RRClient));
}

void
rr_destroy_client (RRClient ** client)
{
  if (!client)
    return;
  if (!(*client))
    return;

  free (*client);
  *client = NULL;
}
