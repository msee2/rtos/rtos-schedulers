/**
 * Electronics Engineering Master's
 * Real Time Operating Systems
 * Author: Luis G. Leon Vega <luis@luisleon.me>
 * Copyright 2021 - MIT
 * Based on Waldspurger PhD Thesis
 */

#include <stdlib.h>
#include <stdio.h>

#include "list.h"
#include "lottery.h"

extern void timer_handler (int sig);

void
lottery_client_join (LotterySched * client, LotteryClient * c, uint tickets,
    Task * job)
{
  if (!client || !c)
    return;

  Node *node = node_create ();
  list_append (&client->ready_poll, node);

  c->tickets = tickets;
  c->task = job;
  c->iters = 0;
  c->num = node->index;

  client->num_tickets += tickets;
  client->num_delivered_tickets += tickets;
  node->data = (void *) c;
};

void
lottery_client_leave (LotterySched * client, Node * node)
{
  if (!node)
    return;
  if (!client)
    return;
  LotteryClient *c;

  c = (LotteryClient *) node->data;
  if (!c)
    goto pop;
  client->num_tickets -= c->tickets;

  free (c);
pop:
  list_pop (&client->ready_poll, node);
}

void
lottery_allocate (LotterySched * client)
{
  size_t winner = 0;
  uint sum = 0;
  LotteryClient *c = NULL;
  Node *node = NULL;
  bool res;
  List *list = NULL;

  if (!client)
    return;
  list = &client->ready_poll;

  /* Check everything */
  if (client->num_tickets <= 0)
    return;

  /* Gets a new winner */
  winner = rand () % client->num_tickets;

  /* Search the winner client */
  for (node = list->head; node != NULL; node = node->next) {
    c = (LotteryClient *) node->data;
    sum += c->tickets;
    /* Check if it's the winner */
    if (sum > winner)
      break;
  }

  /* Act accordingly */
  if (!c->task)
    goto release;

  fprintf (stdout, "Task: %i\n", c->task->id);
  res = job_execute (c->task);

  /* Check termination */
  if (res) {
    goto release;
  }

  return;
release:
  lottery_client_leave (client, node);
  node_delete (&node);

  /* This is not applicable to a real case scenario, since the
     scheduler will wait for threads or NOP threads */
}

/**
 * lottery_create_client:
 * Creates a new client
 */
LotteryClient *
lottery_create_client ()
{
  return (LotteryClient *) calloc (1, sizeof (LotteryClient));
}

/**
 * lottery_destroy_client:
 * Deletes a new client
 */
void
lottery_destroy_client (LotteryClient ** client)
{
  if (!client)
    return;
  if (!(*client))
    return;

  free (*client);
  *client = NULL;
}
