/**
 * Electronics Engineering Master's
 * Real Time Operating Systems
 * Author: Luis G. Leon Vega <luis@luisleon.me>
 * Copyright 2021 - MIT
 */

#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include "timer.h"

TimerSched *
timer_sched_create (sighandler_t handler, Task * task, long d_ns, long i_ns,
    int id)
{
  /* Guard */
  if (!task)
    return NULL;
  if (!handler)
    return NULL;

  /* Create the timer */
  TimerSched *tm = (TimerSched *) calloc (1, sizeof (TimerSched));
  if (!tm)
    return NULL;

  /* Configure */
  tm->task = task;
  tm->id = id;

  /* Configure timer settings */
  tm->timeval.it_value.tv_sec = 0;
  tm->timeval.it_value.tv_nsec = d_ns;
  tm->timeval.it_interval.tv_sec = 0;
  tm->timeval.it_interval.tv_nsec = i_ns;

  /* Configure the signal */
  tm->sigev.sigev_notify = SIGEV_THREAD;
  tm->sigev.sigev_notify_function = handler;
  tm->sigev.sigev_notify_attributes = NULL;
  tm->sigev.sigev_signo = SIGALRM;
  tm->sigev.sigev_value.sival_ptr = (void *) tm;

  return tm;
}

TimerSched *
timer_sig_create (int sig, long d_ns, long i_ns, int id)
{

  /* Create the timer */
  TimerSched *tm = (TimerSched *) calloc (1, sizeof (TimerSched));
  if (!tm)
    return NULL;

  /* Configure */
  tm->id = id;

  /* Configure timer settings */
  tm->timeval.it_value.tv_sec = 0;
  tm->timeval.it_value.tv_nsec = d_ns;
  tm->timeval.it_interval.tv_sec = 0;
  tm->timeval.it_interval.tv_nsec = i_ns;

  /* Configure the signal */
  tm->sigev.sigev_notify = SIGEV_SIGNAL;
  tm->sigev.sigev_signo = sig;
  tm->sigev.sigev_value.sival_ptr = (void *) tm;

  return tm;
}

void
timer_sched_play (TimerSched * timer_ptr)
{
  if (!timer_ptr)
    return;

  /* Deploy */
  timer_create (CLOCK_REALTIME, &(timer_ptr->sigev), &(timer_ptr->timer));
  timer_settime (timer_ptr->timer, 0, &timer_ptr->timeval, NULL);

  timer_ptr->running = true;
}

void
timer_sched_stop (TimerSched * timer_ptr)
{
  if (!timer_ptr)
    return;

  /* Configure timer settings */
  timer_ptr->timeval.it_value.tv_sec = 0;
  timer_ptr->timeval.it_value.tv_nsec = 0;
  timer_ptr->timeval.it_interval.tv_sec = 0;
  timer_ptr->timeval.it_interval.tv_nsec = 0;

  /* Set everything to zero */
  memset ((void *) &(timer_ptr->timeval), 0, sizeof (struct itimerspec));
  timer_settime (timer_ptr->timer, 0, &(timer_ptr->timeval), NULL);
  timer_delete (timer_ptr->timer);

  timer_ptr->running = false;
}

void
timer_sched_delete (TimerSched ** timer_ptr)
{
  if (!timer_ptr)
    return;
  if (!(*timer_ptr))
    return;

  /* Stop the timer before the deletion */
  timer_sched_stop (*timer_ptr);

  /* Free the content and nullify the ptr */
  free (*timer_ptr);
  timer_ptr = NULL;
}

long long
timer_cur_time ()
{
  struct timeval te;
  gettimeofday (&te, NULL);
  long long milliseconds = te.tv_sec * 1000ll + te.tv_usec / 1000;
  return milliseconds;
}
