/**
 * Electronics Engineering Master's
 * Real Time Operating Systems
 * Author: Luis G. Leon Vega <luis@luisleon.me>
 * Copyright 2021 - MIT
 */

#include <stdio.h>
#include <string.h>

#include "configs.h"
#include "gui.h"
#include "job.h"
#include "job_launcher.h"
#include "list.h"
#include "lottery.h"
#include "rr.h"
#include "schedulers.h"
#include "taylor.h"
#include "timer.h"

#define CONFIG_FILE "file.sched"

LotterySched lottery_sch;
RRSched rr_sch;

sigjmp_buf *env;
bool started = false;
int total_tickets = 0;
int n_threads = 0;
int argc_p = 0;
char **argv_p = NULL;

/* Interruption */
void
usersignal_handler (int sigval)
{
  if (started && env)
    siglongjmp (*env, 1);
}

void
timer_handler (int sigval)
{
  if (started && env)
    siglongjmp (*env, 1);
}

bool
scheduler_lot_loop ()
{
  if (sigsetjmp (*env, 1) != 0) {
    fprintf (stdout, "Returning to Scheduler\n");
  } else {
    lottery_allocate (&lottery_sch);
  }
  started = true;

  if (lottery_sch.num_delivered_tickets != total_tickets)
    return true;
  if (lottery_sch.num_tickets == 0)
    return false;
  return true;
}

bool
scheduler_rr_loop ()
{
  if (sigsetjmp (*env, 1) != 0) {
    fprintf (stdout, "Returning to Scheduler\n");
  } else {
    rr_allocate (&rr_sch);
  }
  started = true;

  if (rr_sch.acc_num_threads != n_threads)
    return true;
  if (rr_sch.num_threads == 0)
    return false;
  return true;
}

/* Scheduler main since the GUI is now the focus of the app */
static void *
scheduler_main (gpointer data)
{
  GUIApp *gui = NULL;
  List waiting_list;
  Task *task_array = NULL;
  SchedConfigs *configs = NULL;
  struct sigaction psa = { 0 }, timersa = { 0 };
  TimerSched *tm = NULL;
  schedule_t sch_callback;
  size_t sch_load = 0;
  sigjmp_buf env_;
  void *sched = NULL;
  env = &env_;

  if (!data)
    return NULL;

  gui = (GUIApp *) data;
  task_array = gui->tasks;
  configs = gui->configs;
  if (!task_array || !configs)
    return NULL;

  sch_callback = schedule_get_callback (configs->scheduler);

  /* Launch callback */
  psa.sa_handler = usersignal_handler;
  sigaction (SIGUSR1, &psa, NULL);
  timersa.sa_handler = timer_handler;
  sigaction (SIGUSR2, &timersa, NULL);

  memset (&waiting_list, 0, sizeof (List));

  /* Create new timers */
  sched =
      configs->scheduler == LOT_SCH ? (void *) &lottery_sch : (void *) &rr_sch;
  job_launcher (&waiting_list, sch_callback, sched, task_array,
      configs->n_procs, configs->arr_times, configs->tickets);

  sch_load =
      LOT_SCH ==
      configs->scheduler ? lottery_sch.num_tickets : rr_sch.num_threads;
  fprintf (stdout, "Sch Load: %li\n", sch_load);

  /* Enable timer if it's preemptive */
  if (configs->mode) {
    tm = timer_sig_create (SIGUSR2, configs->quantum, configs->quantum, -1);
    timer_sched_play (tm);
  }

  /* Free list */
  while (1) {
    if (LOT_SCH == configs->scheduler) {
      if (!scheduler_lot_loop ())
        break;
    } else {
      if (!scheduler_rr_loop ())
        break;
    }
  }

  fprintf (stdout, "Finished \n");
  timer_sched_delete (&tm);
  return NULL;
}

static void *
gui_main (gpointer data)
{
  int ret;
  GUIApp *gui = NULL;
  GUIApp *settings = NULL;

  g_return_val_if_fail (data, NULL);
  settings = (GUIApp *) data;

  gui = gui_create_app (argc_p, argv_p);

  if (!gui) {
    fprintf (stderr, "Cannot get the GUI. Exiting...\n");
    return NULL;
  }

  gui->tasks = settings->tasks;
  gui->configs = settings->configs;

  ret = gui_run_app (gui);
  if (ret) {
    fprintf (stderr, "Cannot run the GUI. Exiting...\n");
    goto gui_del;
  }

gui_del:
  gui_free_destroy (&gui);
  return NULL;
}

int
main (int argc, char **argv)
{
  SchedConfigs *configs = NULL;
  bool ret_b = true;
  Task *task_array = NULL;
  int ret = EXIT_SUCCESS;
  GUIApp gui_setting = { 0 };
  GThread *t;

  argc_p = argc;
  argv_p = argv;

  configs = sched_configs_new ();
  ret_b = sched_configs_read (configs, CONFIG_FILE);
  if (!ret_b) {
    fprintf (stderr, "Cannot load settings. Exiting...\n");
    ret = EXIT_FAILURE;
    goto configs_del;
  }
  fprintf (stdin, "Executing with the following settings\n");
  sched_configs_print (configs);

  /* Create tasks and initialise */
  memset (&lottery_sch, 0, sizeof (LotterySched));
  memset (&rr_sch, 0, sizeof (RRSched));
  task_array = (Task *) calloc (configs->n_procs, sizeof (Task));

  for (int i = 0; i < configs->n_procs; ++i) {
    task_array[i].n_jobs = configs->j_load[i];
    task_array[i].n_work_done = configs->mode ? -1 : configs->quantum;
    task_array[i].task = taylor;
    total_tickets += configs->tickets[i];
    ++n_threads;
  }

  gui_setting.tasks = task_array;
  gui_setting.configs = configs;

  t = g_thread_new ("Gui_T", gui_main, &gui_setting);
  scheduler_main (&gui_setting);

  g_thread_join (t);

  free (task_array);
configs_del:
  sched_configs_free (configs);
  return ret;
}
