
/**
 * Electronics Engineering Master's
 * Real Time Operating Systems
 * Author: Luis G. Leon Vega <luis@luisleon.me>
 * Copyright 2021 - MIT
 */

#include "schedulers.h"

#include <stdio.h>

#include "job.h"
#include "lottery.h"
#include "rr.h"

schedule_t
schedule_get_callback (int scheduler)
{
  switch (scheduler) {
    case LOT_SCH:
      return schedule_lottery;
    case RR_SCH:
      return schedule_rr;
    default:
      return NULL;
  }
}

bool
schedule_mode_check (int scheduler, int mode)
{
  switch (scheduler) {
    case RR_SCH:
      if (PE_MOD == mode)
        return true;
      if (NPE_MOD == mode) {
        fprintf (stderr,
            "Non-preemptive mode is not supported in RR scheduler\n");
        return false;
      }
    case LOT_SCH:
      return true;
    default:
      fprintf (stderr, "Invalid scheduler\n");
      return false;
  }
}

void
schedule_lottery (void *sch, Task * job, void *user_data)
{
  LotterySched *lottery_sch;
  LotteryClient *lottery_client;
  uint *tickets_ptr;

  if (!sch || !job || !user_data)
    return;

  lottery_sch = (LotterySched *) sch;
  tickets_ptr = (uint *) user_data;

  lottery_client = lottery_create_client ();
  lottery_client_join (lottery_sch, lottery_client, *tickets_ptr, job);
}

void
schedule_rr (void *sch, Task * job, void *user_data)
{
  RRSched *rr_sch;
  RRClient *rr_client;

  if (!sch || !job)
    return;

  rr_sch = (RRSched *) sch;

  rr_client = rr_create_client ();
  rr_client_join (rr_sch, rr_client, job);
}
