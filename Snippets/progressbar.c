/**
 * Electronics Engineering Master's
 * Real Time Operating Systems
 * Author: Luis G. Leon Vega <luis@luisleon.me>
 * Copyright 2021 - MIT
 */

#include <gtk/gtk.h>

typedef struct _ProgressBar {
  GtkWidget *label;
  GtkWidget *progress_bar;
} ProgressBar;

ProgressBar *global_bar = NULL;

static gboolean
fill (gpointer user_data)
{
  gchar *percentage = NULL;
  gdouble fraction;
  ProgressBar *bar = NULL;
  GtkWidget *progress_bar = NULL;
  GtkWidget *label = NULL;
  
  bar = user_data;
  g_return_val_if_fail(user_data, FALSE);

  progress_bar = bar->progress_bar;
  label = bar->label;
  g_return_val_if_fail(progress_bar, FALSE);
  g_return_val_if_fail(label, FALSE);

  /*Get the current progress */
  fraction = gtk_progress_bar_get_fraction (GTK_PROGRESS_BAR (progress_bar));

  /*Increase the bar by 10% each time this function is called */
  fraction += 0.1;

  /*Fill in the bar with the new fraction */
  gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (progress_bar), fraction);
  percentage = g_strdup_printf(" %i %%", (int)(fraction * 100));
  gtk_label_set_text (GTK_LABEL (label), percentage);
  g_free(percentage);

  /*Ensures that the fraction stays below 1.0 */
  if (fraction < 1.0)
    return TRUE;

  return FALSE;
}



static void
activate (GtkApplication * app, gpointer user_data)
{
  GtkWidget *window;
  GtkWidget *progress_bar;
  GtkWidget *label, *progress_bar_label;
  GtkWidget *grid;

  gdouble fraction = 0.0;

  /*Create a window with a title, and a default size */
  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "ProgressBar Example");
  gtk_window_set_default_size (GTK_WINDOW (window), 300, 50);

  /* Create a table for layout  */
  grid = gtk_grid_new ();
  gtk_container_add (GTK_CONTAINER (window), grid);
  gtk_grid_set_row_spacing (GTK_GRID (grid), 10);

  /* Create the label */
  label = gtk_label_new ("Loading...");
  gtk_grid_attach (GTK_GRID (grid), label, 0, 0, 2, 2);
  gtk_widget_show (label);

  /*Create a progressbar and add it to the window */
  progress_bar = gtk_progress_bar_new ();
  gtk_grid_attach (GTK_GRID (grid), progress_bar, 0, 2, 10, 2);

  progress_bar_label = gtk_label_new (" 0 %");
  gtk_grid_attach_next_to (GTK_GRID (grid), progress_bar_label, progress_bar, GTK_POS_RIGHT, 2, 2);
  gtk_widget_show (progress_bar_label);

  /*Fill in the given fraction of the bar. Has to be between 0.0-1.0 inclusive */
  gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (progress_bar), fraction);

  /*Encapsulate*/
  global_bar = g_new0(ProgressBar, 1);
  global_bar->label = progress_bar_label;
  global_bar->progress_bar = progress_bar;

  /*Use the created fill function every 500 milliseconds */
  g_timeout_add (500, fill, global_bar);

  gtk_widget_show_all (window);
}



int
main (int argc, char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  g_free(global_bar);

  return status;
}
