#
# Electronics Engineering Master's
# Real Time Operating Systems
# Author: Luis G. Leon Vega <luis@luisleon.me>
# Copyright 2021 - MIT
#

COMPILER=gcc
#CFLAGS=-O0 -g -Wall `pkg-config --cflags gtk+-3.0` -Iinclude
CFLAGS=-Ofast -march=native -Werror -Wall `pkg-config --cflags gtk+-3.0` -Iinclude
LIBS=`pkg-config --libs gtk+-3.0` -lrt -lm
SRCS=$(wildcard src/*.c)
OBJS=$(SRCS:.c=.o)
EXE=main

PDF=main.pdf
PDF_COMPILER=pdflatex
PDF_SRC_PATH=docs
PDF_MAIN=$(PDF_SRC_PATH)/$(PDF:.pdf=.tex)
DOC_SRCS=$(wildcard docs/*.tex)

all: $(EXE)

# Executable
$(EXE): $(OBJS)
	$(COMPILER) -o $@ $(OBJS) $(LIBS)

%.o: %.c
	$(COMPILER) -c $< -o $@ $(CFLAGS) $(LIBS)

# Documentation
docs: okular

$(PDF): $(DOC_SRCS)
	$(PDF_COMPILER) $(PDF_MAIN) && $(PDF_COMPILER) $(PDF_MAIN)

okular: $(PDF)
	okular $(PDF)

clean_pdf:
	$(RM) $(PDF) *.aux *.log *.fls *.toc *.snm *.out *.nav

clean: clean_pdf
	$(RM) $(OBJS) $(EXE) src/*~ include/*~

.PHONY: all docs clean
