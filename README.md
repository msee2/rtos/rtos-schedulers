# RTOS Schedulers

## Installation

### Install dependencies

Please, make sure of having the following dependencies fulfiled:

```bash
sudo apt-get install -y libglib2.0 libglib2.0-dev
sudo apt-get install libgtk-3-dev okular
sudo apt-get install texlive-latex-base texlive-fonts-recommended texlive-fonts-extra texlive-latex-extra
```

> Make sure of having gtk-3 in its 3.24 version! You can check the version by typing: `apt-cache policy libgtk-3-0`

### Building and Compiling

After fulfiling the dependencies, build the project:

```bash
make
```

For building the docs, please, use the following command:

```bash
make docs
```

It will generate a binary called: `main`, it is the executable of the project.

### Missing

* Slide with tasks
* Table with Conclusions

Author:

* Luis G Leon Vega <lleon95@gmail.com>
* Jean Paul Jimenez

v0.1.0
