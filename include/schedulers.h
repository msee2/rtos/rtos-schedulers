/*
  Instituto Tecnologico de Costa Rica
  Master in Electronics Engineering
  RTOS - I-2021
  Author:
    - Jean P. Jimenez
    - Luis G Leon Vega <lleon95@gmail.com>
*/

#ifndef __SCHEDULERS_H__
#define __SCHEDULERS_H__

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stdbool.h>

#include <gtk/gtk.h>

#include "structures.h"

#define NOTASK   0              // ID when there is no tasks ready (Subutilization)
#define READY    0
#define FINISHED 1

/*----------------- Helpers -----------------*/

/**
 * Compute the greater common divisor between a and b
 * @param a a number
 * @param b a number
 */
gint gcd (gint a, gint b);

/**
 * Compute the least common multiplier between a and b
 * @param task task array
 * @param n number of tasks within the pool
 */
gint lcm (TaskConfig * tasks, gint n);

/*----------------- Checks -----------------*/
/**
 * Compute actual computation
 * @param task task array
 * @param n_tasks number of tasks within the pool
 */
gdouble compute_utilisation (TaskConfig * tasks, gint n_tasks);

/**
 * Compute max computation
 * @param task task array
 * @param n_tasks number of tasks within the pool
 */
gdouble compute_max_utilisation (TaskConfig * tasks, gint n_tasks);
/**
 * Checks the RM schedulability using Linear criterion
 * @param task task array
 * @param n_tasks number of tasks within the pool
 */
gboolean rm_check_lub (TaskConfig * task, gint n_tasks);

/**
 * Checks the RM schedulability using Hyperbolic criterion
 * @param task task array
 * @param n_tasks number of tasks within the pool
 */
gboolean rm_check_hb (TaskConfig * task, gint n_tasks);

/**
 * Checks the EDF schedulability
 * @param task task array
 * @param n_tasks number of tasks within the pool
 */
gboolean edf_check (TaskConfig * tasks, gint n_tasks);

/*----------------- Simulation runners -----------------*/

/**
 * Run a Rate Monotonic Simulation
 * @param tasks task array to run
 * @param n_tasks number of tasks within the array
 * @param sim simulation object to load the results in
 * @param simulation_time simulation time to execute (greater than 0)
 */
void run_rm_simulation (TaskConfig * tasks, gint n_tasks, Simulation * sim,
    gint simulation_time);

/**
 * Run an Earliest Deadline First Simulation
 * @param tasks task array to run
 * @param n_tasks number of tasks within the array
 * @param sim simulation object to load the results in
 * @param simulation_time simulation time to execute (greater than 0)
 */
void run_edf_simulation (TaskConfig * tasks, gint n_tasks, Simulation * sim,
    gint simulation_time);

/**
 * Run a Least Laxity First Simulation
 * @param tasks task array to run
 * @param n_tasks number of tasks within the array
 * @param sim simulation object to load the results in
 * @param simulation_time simulation time to execute (greater than 0)
 */
void run_llf_simulation (TaskConfig * tasks, gint n_tasks, Simulation * sim,
    gint simulation_time);

/*----------------- Printers -----------------*/

/**
 * Prints tasks in stdout
 * @param task task array
 * @param n_tasks number of tasks within the pool
 */
void print_tasks (TaskConfig * tasks, gint n_tasks);

/**
 * Prints simulation in stdout
 * @param task task array
 * @param n_tasks number of tasks within the pool
 */
void print_sim (Simulation * sim, gint sim_time);

#endif /* __SCHEDULERS_H__ */
