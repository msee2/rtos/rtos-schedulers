/*
  Instituto Tecnologico de Costa Rica
  Master in Electronics Engineering
  RTOS - I-2021
  Author:
    - Luis G Leon Vega <lleon95@gmail.com>
*/

#ifndef __RUNNER_H__
#define __RUNNER_H__

#include <gtk/gtk.h>

/**
 * Function invoked when the Start button is clicked
 * @param widget the button
 * @param data the GUI object
 */
void on_start_execution (GtkWidget * widget, gpointer * data);

/**
 * Function invoked when the Start button is clicked
 * @param widget the button
 * @param data the GUI object
 */
void on_check_algorithms (GtkWidget * widget, gpointer * data);

#endif /* __RUNNER_H__ */
