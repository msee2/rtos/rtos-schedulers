/*
  Instituto Tecnologico de Costa Rica
  Master in Electronics Engineering
  RTOS - I-2021
  Author:
    - Luis G Leon Vega <lleon95@gmail.com>
*/

#ifndef __STRUCTURES_H__
#define __STRUCTURES_H__

#include <gtk/gtk.h>

#include "configs.h"

/**
 * Task object
 * It is the fundamental definition of the task
 */
typedef struct
{
  gint id;
  gint period;
  gint computation_time;
  gint remaining_time;
  gint deadline;
  gint laxity;
  gint state;
} TaskConfig;

typedef struct
{
  gint *ids;
  gint *missed_deadlines;
  gint *missed_ids;
  gint *periods;
  gint count;
  TaskConfig *tasks;
} Simulation;

/**
 * AnalysisConfig object
 * It contains information about the configuration in the GUI and information
 * about schedulability
 */
typedef struct
{
  /* Scheduler selection */
  gboolean rm_scheduler;
  gboolean edf_scheduler;
  gboolean llf_scheduler;

  /* Presentation mode */
  gboolean show_per_slide;
  gboolean animate;
  TaskConfig configs[MAX_TASKS];

  /* Properties */
  guint minimum_multiplier;
  gfloat u_n;
  gfloat processing_load;

  /* Results */
  gboolean rm_schedulable;
  gboolean edf_schedulable;
  gboolean llf_schedulable;

  /* Sim Results */
  gboolean sim_rm_schedulable;
  gboolean sim_edf_schedulable;
  gboolean sim_llf_schedulable;
} AnalysisConfig;

#endif /* __STRUCTURES_H__ */
