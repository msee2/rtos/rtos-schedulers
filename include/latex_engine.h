/*
  Instituto Tecnologico de Costa Rica
  Master in Electronics Engineering
  RTOS - I-2021
  Author:
    - Luis G Leon Vega <lleon95@gmail.com>
*/

#ifndef __LATEX_ENGINE_H__
#define __LATEX_ENGINE_H__

#include "gui.h"
#include "structures.h"

/* Maximum file size in bytes */
#define FILE_SIZE 5000
/* Maximum template field size */
#define FIELD_SIZE 14

/* Flags */
#define ENABLE_RMT 0b1
#define ENABLE_EDF 0b10
#define ENABLE_LLF 0b100
#define ENABLE_WRP 0b1000

#define EXECUTION_DOC "docs/execution.tex"

/* Creating a new frame */
#define FRAME_BEGIN_SC(x, y) "\\begin{frame}{" x "}\n" \
                       "\\scalebox{" y "}{\\begin{minipage}{0.8\\linewidth}"
#define FRAME_END_SC "\n\\end{minipage}}\\end{frame}\n"
#define FRAME_BEGIN(x) "\\begin{frame}{" x "}\n"
#define FRAME_END "\n\\end{frame}\n"
#define NUMBER_SLICES_ROW 24
#define NUMBER_ROWS 3
#define TOTAL_CHARS 102400000
#define BUFFER_SIZE 10000000

/* Create a new table */
#define TABLE_BEGIN_PTT "\\begin{table}[]\n"\
                         "\\begin{tabular}{%s}\n"\
                         "\\hline\n"
#define TABLE_BEGIN_PTT_WITH_CAP "\\begin{table}[]\n" \
                                 "\\caption{%s}\n" \
                                 "\\begin{tabular}{%s}\n" \
                                 "\\hline\n"
#define TABLE_END "\\end{tabular}\n\\end{table}\n"
#define COL_SEPARATOR " & "
#define ROW_SEPARATOR " \\\\ \\hline\n"
#define SEPARATORS_MAX_LEN 56
#define CAPTION "\\caption{%s} \\\\"

/**
 * Write the result (file)
 * @param filename filename to write
 * @param content_buffer buffer to write
 */
int write_result (const char *filename, const char *content_buffer);

/**
 * Write the result for the RM schedulability test
 * @param mu actual utilisation
 * @param u limit utilisation
 */
int result_rm_test (const float mu, const float u);

/**
 * Write the result for the EDF schedulability test
 * @param mu actual utilisation
 */
int result_edf_test (const float mu);

/**
 * Write the result for the LLF schedulability test
 * @param mu actual utilisation
 */
int result_llf_test (const float mu);

/**
 * Enables and writes the result file
 * @param flags enable options
 * @param filename filename to get as a template and write back
 */
int enable_test_slide (const int flags, const char *filename);

/**
 * Creates the separators for the tables
 * @param separators output buffer with the separators
 * @param slice_idx slice index of the table
 * @param num_slices number of slices
 */
void create_separators (char separators[SEPARATORS_MAX_LEN],
    const int slice_idx, const int num_slices);

/**
 * Execution frame creator
 * @param sim Simulation object
 * @param num_tasks number of tasks in the array
 * @param num_slices number of slices in time
 * @param filename filename to printout the results
 */
void create_execution_frame (Simulation * sim, const int num_tasks,
    const int num_slices, const char *filename);


void
create_wrap_execution_frame (AnalysisConfig * config,
    Simulation * sim_rm,
    Simulation * sim_edf,
    Simulation * sim_llf,
    const int num_tasks, const int num_slices, const char *filename);

/**
 * Task creator
 * @param gui gui object
 * @param num_tasks number of tasks in the array
 * @param final_res print the final results or not
 * @param filename filename to printout the results
 */
void create_tasks_frame (Gui * gui, const int num_tasks, const int final_res,
    const char *filename);

/**
 * Create a presentation by invoking the building system
 */
int make_presentation ();

/**
 * Resorts the tasks to its original ordering
 */
void resort_tasks (TaskConfig * resort, TaskConfig * unsort);

#endif /* __LATEX_ENGINE_H__ */
