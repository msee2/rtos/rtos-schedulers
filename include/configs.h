/*
  Instituto Tecnologico de Costa Rica
  Master in Electronics Engineering
  RTOS - I-2021
  Author:
    - Luis G Leon Vega <lleon95@gmail.com>
*/

#ifndef __CONFIGS_H__
#define __CONFIGS_H__

//#define DEBUG
#define PRINT_SIMS_STDOUT
#define MAX_TASKS 6
#define MIN_TASKS 1
#define rm_check rm_check_lub

#endif /* __CONFIGS_H__ */
