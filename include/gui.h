/*
  Instituto Tecnologico de Costa Rica
  Master in Electronics Engineering
  RTOS - I-2021
  Author:
    - Luis G Leon Vega <lleon95@gmail.com>
*/

#ifndef __GUI_H__
#define __GUI_H__

#include <gtk/gtk.h>

#include "configs.h"
#include "structures.h"

/**
 * GUI object: it contains several objects used in the project, including
 * widgets, task arrays, task configs, and the window builder
 */
typedef struct
{
  GtkWindow *window;
  GtkBuilder *builder;
  guint n_tasks;
  GList *task_rows;
  GMutex lock;
  AnalysisConfig analysis;
  Simulation simulation;
} Gui;

/*----------------- Helpers -----------------*/

/**
 * Gets all the actions and values from form
 * @param gui the GUI object
 */
void get_selected_options (Gui * gui);

/**
 * Sets the labels with the information about the CPU utilisation and LMC
 * @param gui the GUI object
 */
void show_common_values (Gui * gui);

/**
 * Sets the task configuration within the config array
 * @param gui the GUI object
 * @param config the tasks array with the configs
 * @param index the task index
 */
void set_task_config (Gui * gui, TaskConfig * config, guint index);

/**
 * Initialises the button and connect them to the signals
 * @param gui the GUI object
 */
void init_buttons (Gui * gui);

/**
 * Initialises the task array
 * @param gui the GUI object
 */
void init_task_array (Gui * gui);

/*----------------- Signals -----------------*/

/**
 * Function invoked when the remove task button is clicked
 * @param widget the button
 * @param data the GUI object
 */
void on_remove_task (GtkWidget * widget, gpointer * data);

/**
 * Function invoked when the add task button is clicked
 * @param widget the button
 * @param data the GUI object
 */
void on_add_new_task (GtkWidget * widget, gpointer * data);

/*----------------- Trivial functions  -----------------*/

/**
 * Validates that the period is greater or equal than the compute time
 * @param config task to validate
 */
inline static gboolean
validate_entry (TaskConfig * config)
{
  gint res = TRUE;

  g_return_val_if_fail (config, FALSE);

  res &= config->period >= config->computation_time;
  res &= config->period > 0;
  res &= config->computation_time > 0;

  return res;
}

#endif /* __GUI_H__ */
