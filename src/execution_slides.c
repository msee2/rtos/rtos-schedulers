/*
  Instituto Tecnologico de Costa Rica
  Master in Electronics Engineering
  RTOS - I-2021
  Author:
    - Luis G Leon Vega <lleon95@gmail.com>
*/

#include "latex_engine.h"

#include <stdio.h>
#include <string.h>

/* Define colours */
#define COLOUR_RACK "\\cellcolor[HTML]{%s}"
#define COLOUR_RACK_ARR "\\cellcolor[HTML]{%s} %i"
static char COLOURS[8][7] = {
  "FFFFFF\0",
  "00FF00\0",
  "0000FF\0",
  "FF0000\0",
  "FFFF00\0",
  "00FFFF\0",
  "FF00FF\0",
  "000000\0"
};

static char COLOURS_DEADLINE[8][7] = {
  "FFFFFF\0",
  "003300\0",
  "000033\0",
  "330000\0",
  "333300\0",
  "003333\0",
  "330033\0",
  "000000\0"
};

#define TASK_ID_MISSED 7
#define SCALE "0.60"

void
create_separators (char separators[SEPARATORS_MAX_LEN], const int slice_idx,
    const int num_slices)
{
  int i = 0;
  memset (separators, 0, SEPARATORS_MAX_LEN);
  if (num_slices - slice_idx > 0) {
    strcat (separators, "|l|");
    for (i = 0; i < NUMBER_SLICES_ROW; ++i) {
      strcat (separators, "l");
      strcat (separators, "|");
    }
  }
}

void
resort_tasks (TaskConfig * resort, TaskConfig * unsort)
{
  int i = 0;

  if (!resort || !unsort)
    return;

  for (i = 0; i < MAX_TASKS; ++i) {
    memcpy(&resort[unsort[i].id - 1], &unsort[i], sizeof(TaskConfig));
  }
}

void
create_execution_frame (Simulation * sim, const int num_tasks,
    const int num_slices, const char *filename)
{
  int frame_idx = 0, row_idx = 0, task_idx = 0, slice_idx = 0, idx = 0;
  int total_frames = 0, total_rows = 0, deadlines = 0;
  int task_id = 0, task_exec = 0, slide_top = 0, stop = 0;
  gboolean arrival = FALSE;
  char *colour = NULL;
  TaskConfig resorted_tasks[MAX_TASKS] = { 0 };

  if (!sim || !filename)
    return;

  char *frame = (char *) calloc (TOTAL_CHARS, 1);
  char *buffer = (char *) calloc (BUFFER_SIZE, 1);
  char separators[SEPARATORS_MAX_LEN] = "";

  total_rows = num_slices / NUMBER_SLICES_ROW + 1;
  total_frames = total_rows / NUMBER_ROWS + 1;

  resort_tasks (resorted_tasks, sim->tasks);

  /* Frame loop */
  for (frame_idx = 0; frame_idx < total_frames; ++frame_idx) {
    if (idx >= num_slices || stop)
      break;
    strcat (frame, FRAME_BEGIN_SC ("Test", SCALE));

    /* Row loop */
    for (row_idx = 0; row_idx < NUMBER_ROWS; ++row_idx) {
      /* If all the slices are filled, no more work to do */
      if (idx >= num_slices || stop)
        break;
      sprintf (buffer, "Starting time from: $t = %i$", idx);
      strcat (frame, buffer);
      memset ((void *) buffer, 0, BUFFER_SIZE);
      create_separators (separators, idx, num_slices);
      sprintf (buffer, TABLE_BEGIN_PTT, separators);
      strcat (frame, buffer);

      /* Task loop */
      slide_top = idx;
      for (task_idx = 0; task_idx < num_tasks; ++task_idx) {
        idx = slide_top;
        memset ((void *) buffer, 0, BUFFER_SIZE);
        sprintf (buffer, "Task %i", task_idx + 1);
        strcat (frame, buffer);

        /* Slice loop */
        for (slice_idx = 0; slice_idx < NUMBER_SLICES_ROW; ++slice_idx) {
          if (idx >= num_slices)
            break;
          strcat (frame, COL_SEPARATOR);
          memset ((void *) buffer, 0, BUFFER_SIZE);

          task_id = task_idx + 1;
          arrival = !(idx % resorted_tasks[task_idx].period);

          if (sim->missed_deadlines[deadlines] == idx
              && sim->missed_ids[deadlines] == task_id) {
            colour = COLOURS_DEADLINE[task_id];
            if (deadlines < sim->count)
              ++deadlines;
            stop = 1;
          } else {
            task_exec = sim->ids[idx];
            colour = task_exec == task_id ? COLOURS[task_id] : COLOURS[0];
          }

          if (arrival)
            sprintf (buffer, COLOUR_RACK_ARR, colour, task_id);
          else
            sprintf (buffer, COLOUR_RACK, colour);

          strcat (frame, buffer);
          ++idx;
        }
        strcat (frame, ROW_SEPARATOR);
      }
      idx = slide_top + slice_idx;

      strcat (frame, TABLE_END);
    }
    strcat (frame, FRAME_END_SC);
  }

  write_result (filename, frame);

  free (frame);
  free (buffer);
}
