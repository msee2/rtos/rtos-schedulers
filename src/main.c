/*
  Instituto Tecnologico de Costa Rica
  Master in Electronics Engineering
  RTOS - I-2021
  Author:
    - Jean P. Jimenez
    - Luis G Leon Vega <lleon95@gmail.com>
*/

#include "configs.h"
#include "gui.h"
#include "schedulers.h"
#include "structures.h"

static void
on_window_main_destroy ()
{
  gtk_main_quit ();
}

int
main (int argc, char *argv[])
{
  GtkBuilder *builder = NULL;
  GtkWidget *window = NULL;
  Gui gui;

  /* Structure initialisation */
  gui.n_tasks = 1;
  gui.task_rows = NULL;
  memset ((void *) &(gui.analysis), 0, sizeof (AnalysisConfig));
  memset ((void *) &(gui.simulation), 0, sizeof (Simulation));

  gtk_init (&argc, &argv);

  builder = gtk_builder_new ();
  gtk_builder_add_from_file (builder, "resources/gui.glade", NULL);
  gui.builder = builder;
  g_mutex_init (&(gui.lock));

  /* Connect the callback for the Windows */
  window = GTK_WIDGET (gtk_builder_get_object (builder, "window"));
  g_signal_connect (window, "destroy", G_CALLBACK (on_window_main_destroy),
      NULL);
  gui.window = GTK_WINDOW (window);

  /* Initialise the tasks */
  init_task_array (&gui);
  init_buttons (&gui);

  /* Show the window */
  gtk_widget_show (window);
  gtk_main ();

  /* Unreference the dynamic objects */
  g_object_unref (builder);
  g_mutex_clear (&(gui.lock));
  g_list_free (gui.task_rows);

  return 0;
}
