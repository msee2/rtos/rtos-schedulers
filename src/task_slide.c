/*
  Instituto Tecnologico de Costa Rica
  Master in Electronics Engineering
  RTOS - I-2021
  Author:
    - Luis G Leon Vega <lleon95@gmail.com>
*/

#include "latex_engine.h"

#include <stdio.h>
#include <string.h>

#define BUFFER_SIZE_TASKS 1000

void
create_tasks_frame (Gui * gui, const int num_tasks, const int final_res,
    const char *filename)
{
  AnalysisConfig *analysis_config = NULL;
  TaskConfig *task_config = NULL;
  char *frame = NULL, *buffer = NULL;
  unsigned int n_tasks = 0, i = 0;

  if (!gui || !filename) {
    fprintf (stdout, "Exiting...\n");
    return;
  }

  frame = (char *) calloc (TOTAL_CHARS, 1);
  buffer = (char *) calloc (BUFFER_SIZE_TASKS, 1);

  analysis_config = &(gui->analysis);
  task_config = analysis_config->configs;
  n_tasks = gui->n_tasks;

  /* Initialise the table */
  strcat (frame, FRAME_BEGIN ("Task information"));
  sprintf (buffer, TABLE_BEGIN_PTT_WITH_CAP, "Tasks under test", "|l|l|l|");
  strcat (frame, buffer);
  memset ((void *) buffer, 0, BUFFER_SIZE_TASKS);

  /* Create the header */
  strcat (frame, "\\textbf{Task}");
  strcat (frame, COL_SEPARATOR);
  strcat (frame, "\\textbf{Compute time}");
  strcat (frame, COL_SEPARATOR);
  strcat (frame, "\\textbf{Period}");
  strcat (frame, ROW_SEPARATOR);

  /* Generate table */
  for (i = 0; i < n_tasks; ++i) {
    sprintf (buffer, "Task %i & %i & %i", task_config[i].id,
        task_config[i].computation_time, task_config[i].period);
    strcat (frame, buffer);
    memset ((void *) buffer, 0, BUFFER_SIZE_TASKS);
    strcat (frame, ROW_SEPARATOR);
  }
  strcat (frame, TABLE_END);

  /* Place the results */
  if (final_res) {
    strcat (frame, "\n\n");
    sprintf (buffer, "Equation says: %s %s %s\n\n",
        analysis_config->rm_schedulable ? "RM" : "",
        analysis_config->edf_schedulable ? " EDF" : "",
        analysis_config->llf_schedulable ? " LLF" : "");
    strcat (frame, buffer);
    memset ((void *) buffer, 0, BUFFER_SIZE_TASKS);
    sprintf (buffer, "Simulation says: %s %s %s\n\n",
        analysis_config->sim_rm_schedulable ? "RM" : "",
        analysis_config->sim_edf_schedulable ? " EDF" : "",
        analysis_config->sim_llf_schedulable ? " LLF" : "");
    strcat (frame, buffer);
  }

  /* Finalise */
  strcat (frame, FRAME_END);

  write_result (filename, frame);

  free (frame);
  free (buffer);
}
