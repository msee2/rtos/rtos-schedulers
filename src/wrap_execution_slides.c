/*
  Instituto Tecnologico de Costa Rica
  Master in Electronics Engineering
  RTOS - I-2021
  Author:
    - Luis G Leon Vega <lleon95@gmail.com>
*/

#include "latex_engine.h"

#include <stdio.h>
#include <string.h>

/* Define colours */
#define COLOUR_RACK "\\cellcolor[HTML]{%s}"
#define COLOUR_RACK_ARR "\\cellcolor[HTML]{%s} %i"
static char COLOURS[8][7] = {
  "FFFFFF\0",
  "00FF00\0",
  "0000FF\0",
  "FF0000\0",
  "FFFF00\0",
  "00FFFF\0",
  "FF00FF\0",
  "000000\0"
};

static char COLOURS_DEADLINE[8][7] = {
  "FFFFFF\0",
  "003300\0",
  "000033\0",
  "330000\0",
  "333300\0",
  "003333\0",
  "330033\0",
  "000000\0"
};

#define TASK_ID_MISSED 7
#define NUM_SCHEDULERS 3
#define NAME_LEN 100
#define SCALE "0.55"

void
create_wrap_execution_frame (AnalysisConfig * config,
    Simulation * sim_rm,
    Simulation * sim_edf,
    Simulation * sim_llf,
    const int num_tasks, const int num_slices, const char *filename)
{
  int frame_idx = 0, row_idx = 0, task_idx = 0, slice_idx = 0, idx = 0;
  int total_frames = 0, total_rows = 0, deadlines = 0;
  int task_id = 0, task_exec = 0, slide_top = 0, stop = 0;
  gboolean arrival = FALSE;
  char *colour = NULL;
  Simulation *sim = NULL;
  TaskConfig *task_config;

  if (!sim_rm || !sim_edf || !sim_llf || !config || !filename)
    return;

  char *frame = (char *) calloc (TOTAL_CHARS, 1);
  char *buffer = (char *) calloc (BUFFER_SIZE, 1);
  char separators[SEPARATORS_MAX_LEN] = "";
  char algorithm[NAME_LEN] = "";

  task_config = config->configs; 

  total_rows = (NUM_SCHEDULERS * num_slices) / NUMBER_SLICES_ROW + 1;
  total_frames = total_rows / NUMBER_ROWS + 1;

  /* Frame loop */
  for (frame_idx = 0; frame_idx < total_frames; ++frame_idx) {
    if (idx >= num_slices || stop)
      break;
    strcat (frame, FRAME_BEGIN_SC ("Test", SCALE));

    /* Row loop - NUM_SCHEDULERS must be 3!!!! */
    for (row_idx = 0; row_idx < NUM_SCHEDULERS; ++row_idx) {
      /* Print the message just once */
      if (!row_idx)
        sprintf (buffer, "Starting time from: $t = %i$", idx);
      /* Initialise the sim according to the number of rows */
      switch (row_idx) {
        case 0:
          if (!config->rm_scheduler)
            continue;
          memset ((void *) algorithm, 0, NAME_LEN);
          sim = sim_rm;
          strcat (algorithm, "RM");
          break;
        case 1:
          if (!config->edf_scheduler)
            continue;
          memset ((void *) algorithm, 0, NAME_LEN);
          sim = sim_edf;
          strcat (algorithm, "EDF");
          break;
        case 2:
          if (!config->llf_scheduler)
            continue;
          memset ((void *) algorithm, 0, NAME_LEN);
          sim = sim_llf;
          strcat (algorithm, "LLF");
          break;
        default:
          perror ("The Row index is bigger than 2\n");
          break;
      }

      strcat (frame, buffer);
      memset ((void *) buffer, 0, BUFFER_SIZE);
      create_separators (separators, idx, num_slices);
      sprintf (buffer, TABLE_BEGIN_PTT_WITH_CAP, algorithm, separators);
      strcat (frame, buffer);

      /* Task loop */
      slide_top = idx;
      for (task_idx = 0; task_idx < num_tasks; ++task_idx) {
        idx = slide_top;
        memset ((void *) buffer, 0, BUFFER_SIZE);
        sprintf (buffer, "Task %i", task_idx + 1);
        strcat (frame, buffer);

        /* Slice loop */
        for (slice_idx = 0; slice_idx < NUMBER_SLICES_ROW; ++slice_idx) {
          if (idx >= num_slices)
            break;
          strcat (frame, COL_SEPARATOR);
          memset ((void *) buffer, 0, BUFFER_SIZE);

          task_id = task_idx + 1;
          arrival = !(idx % task_config[task_idx].period);

          if (sim->missed_deadlines[deadlines] == idx
              && sim->missed_ids[deadlines] == task_id) {
            colour = COLOURS_DEADLINE[task_id];
            if (deadlines < sim->count)
              ++deadlines;
            stop = 1;
          } else {
            task_exec = sim->ids[idx];
            colour = task_exec == task_id ? COLOURS[task_id] : COLOURS[0];
          }

          if (arrival)
            sprintf (buffer, COLOUR_RACK_ARR, colour, task_id);
          else
            sprintf (buffer, COLOUR_RACK, colour);

          strcat (frame, buffer);
          ++idx;
        }
        strcat (frame, ROW_SEPARATOR);
      }
      idx = slide_top;          /* Revert */
      strcat (frame, TABLE_END);
    }
    /* The reset occurs when having a new frame */
    idx = slide_top + slice_idx;
    strcat (frame, FRAME_END_SC);
  }

  write_result (filename, frame);

  free (frame);
  free (buffer);
}
