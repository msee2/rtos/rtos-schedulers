/*
  Instituto Tecnologico de Costa Rica
  Master in Electronics Engineering
  RTOS - I-2021
  Author:
    - Luis G Leon Vega <lleon95@gmail.com>
*/

#include "gui.h"

#include <string.h>
#include <math.h>

#include "runner.h"

void
init_task_array (Gui * gui)
{
  GtkWidget *row = NULL;
  int i = MIN_TASKS;
  gchar *row_str = NULL;

  g_return_if_fail (gui);

  g_mutex_lock (&gui->lock);
  for (i = MIN_TASKS; i <= MAX_TASKS; ++i) {
    row_str = g_strdup_printf ("task-row-%i", i);

    row = GTK_WIDGET (gtk_builder_get_object (gui->builder, row_str));
    gui->task_rows = g_list_append (gui->task_rows, row);
    gui->analysis.configs[i - 1].id = i;

    if (row_str) {
      g_free (row_str);
      row_str = NULL;
    }
  }
  g_mutex_unlock (&gui->lock);
}

void
init_buttons (Gui * gui)
{
  GtkWidget *button;

  g_return_if_fail (gui);

  button = GTK_WIDGET (gtk_builder_get_object (gui->builder, "add-task-btn"));
  g_signal_connect (button, "clicked", G_CALLBACK (on_add_new_task),
      (gpointer) (gui));

  button =
      GTK_WIDGET (gtk_builder_get_object (gui->builder, "remove-task-btn"));
  g_signal_connect (button, "clicked", G_CALLBACK (on_remove_task),
      (gpointer) (gui));

  button = GTK_WIDGET (gtk_builder_get_object (gui->builder, "start-btn"));
  g_signal_connect (button, "clicked", G_CALLBACK (on_start_execution),
      (gpointer) (gui));

  button = GTK_WIDGET (gtk_builder_get_object (gui->builder, "check-btn"));
  g_signal_connect (button, "clicked", G_CALLBACK (on_check_algorithms),
      (gpointer) (gui));
}

void
on_add_new_task (GtkWidget * widget, gpointer * data)
{
  Gui *gui = NULL;
  GtkWidget *row = NULL;

  gui = (Gui *) (data);
  g_return_if_fail (gui);

  g_mutex_lock (&gui->lock);

  if (gui->n_tasks == MAX_TASKS) {
    gui->n_tasks = MAX_TASKS;
  } else {
    gui->n_tasks++;
  }
  row = GTK_WIDGET (g_list_nth_data (gui->task_rows, gui->n_tasks - 1));
  gtk_widget_set_visible (row, TRUE);

  g_mutex_unlock (&gui->lock);
}

void
on_remove_task (GtkWidget * widget, gpointer * data)
{
  Gui *gui = NULL;
  GtkWidget *row = NULL;

  g_return_if_fail (data);
  gui = (Gui *) (data);

  g_mutex_lock (&gui->lock);
  if (gui->n_tasks == MIN_TASKS) {
    gui->n_tasks = MIN_TASKS;
  } else {
    gui->n_tasks--;
  }

  row = GTK_WIDGET (g_list_nth_data (gui->task_rows, gui->n_tasks));
  gtk_widget_set_visible (row, FALSE);

  g_mutex_unlock (&gui->lock);
}

inline void
set_task_config (Gui * gui, TaskConfig * config, guint index)
{
  gchar *ci_field_str = NULL;
  gchar *pi_field_str = NULL;
  GtkWidget *c_field = NULL;
  GtkWidget *p_field = NULL;

  g_return_if_fail (config);
  g_return_if_fail (gui);

  index++;

  ci_field_str = g_strdup_printf ("t%i-c", index);
  pi_field_str = g_strdup_printf ("t%i-p", index);

  c_field = GTK_WIDGET (gtk_builder_get_object (gui->builder, ci_field_str));
  p_field = GTK_WIDGET (gtk_builder_get_object (gui->builder, pi_field_str));

  config->computation_time = atoi (gtk_entry_get_text (GTK_ENTRY (c_field)));
  config->period = atoi (gtk_entry_get_text (GTK_ENTRY (p_field)));

#ifdef DEBUG
  g_print ("C: %i, P: %i\n", config->computation_time, config->period);
#endif

  g_free (ci_field_str);
  g_free (pi_field_str);
}

inline void
show_common_values (Gui * gui)
{
  GtkWidget *label;
  gchar *val_str;

  g_return_if_fail (gui);

  /* Get the LCM */
  label = GTK_WIDGET (gtk_builder_get_object (gui->builder, "mmc-lbl"));
  val_str = g_strdup_printf ("%u", gui->analysis.minimum_multiplier);
  gtk_label_set_text (GTK_LABEL (label), val_str);
  g_free (val_str);

  label = GTK_WIDGET (gtk_builder_get_object (gui->builder, "u-lbl"));
  val_str = g_strdup_printf ("%3.5f", gui->analysis.u_n);
  gtk_label_set_text (GTK_LABEL (label), val_str);
  g_free (val_str);

  label = GTK_WIDGET (gtk_builder_get_object (gui->builder, "miu-lbl"));
  val_str = g_strdup_printf ("%3.5f", gui->analysis.processing_load);
  gtk_label_set_text (GTK_LABEL (label), val_str);
  g_free (val_str);
}

void
get_selected_options (Gui * gui)
{
  AnalysisConfig *analysis_config = NULL;
  GtkWidget *toggle = NULL;
  GtkWidget *label = NULL;
  gchar *val_str = NULL;

  g_return_if_fail (gui);
  analysis_config = &(gui->analysis);

  toggle = GTK_WIDGET (gtk_builder_get_object (gui->builder, "rm-chk"));
  analysis_config->rm_scheduler =
      gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle));
  toggle = GTK_WIDGET (gtk_builder_get_object (gui->builder, "edf-chk"));
  analysis_config->edf_scheduler =
      gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle));
  toggle = GTK_WIDGET (gtk_builder_get_object (gui->builder, "llf-chk"));
  analysis_config->llf_scheduler =
      gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle));

  /* Schedulers */
  label = GTK_WIDGET (gtk_builder_get_object (gui->builder, "schd-lbl"));
  val_str = g_strdup_printf ("%s%s%s",
      analysis_config->rm_scheduler ? "RM" : "",
      analysis_config->edf_scheduler ? " EDF" : "",
      analysis_config->llf_scheduler ? " LLF" : "");
  gtk_label_set_text (GTK_LABEL (label), val_str);

  /* Slide options */
  toggle =
      GTK_WIDGET (gtk_builder_get_object (gui->builder, "singleslide-chk"));
  analysis_config->show_per_slide =
      gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle));
  toggle = GTK_WIDGET (gtk_builder_get_object (gui->builder, "animated-chk"));
  analysis_config->animate =
      gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle));

  g_free (val_str);
}
