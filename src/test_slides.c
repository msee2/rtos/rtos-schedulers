/*
  Instituto Tecnologico de Costa Rica
  Master in Electronics Engineering
  RTOS - I-2021
  Author:
    - Luis G Leon Vega <lleon95@gmail.com>
*/

#include "latex_engine.h"

#include <stdio.h>
#include <string.h>

static int
replace_val (char *content_buffer, const char *pattern, const float val)
{
  char *field = NULL;
  char result[FIELD_SIZE] = "";
  char *incipit = NULL, *excipit = NULL, *subpart = NULL;

  incipit = (char *) calloc (FILE_SIZE, sizeof (char));
  excipit = (char *) calloc (FILE_SIZE, sizeof (char));

  field = strstr (content_buffer, pattern);
  if (!field) {
    return -1;
  }
  subpart = field + FIELD_SIZE;

  memset (result, 0, FIELD_SIZE);
  sprintf (result, "%3.5f", val);
  strncpy (field, result, FIELD_SIZE);

  /* Avoid memory overlapping - this can be enhanced anyway */
  memcpy (incipit, content_buffer, field - content_buffer);
  memcpy (excipit, subpart, &content_buffer[FILE_SIZE - 1] - subpart);

  memset (content_buffer, 0, FILE_SIZE);

  /* Complete the file */
  strcat (content_buffer, incipit);
  strcat (content_buffer, result);
  strcat (content_buffer, excipit);

  free (incipit);
  free (excipit);

  return 0;
}

static int
replace_str (char *content_buffer, const char *pattern, const char *val)
{
  char *field = NULL;
  char *incipit = NULL, *excipit = NULL, *subpart = NULL;

  incipit = (char *) calloc (FILE_SIZE, sizeof (char));
  excipit = (char *) calloc (FILE_SIZE, sizeof (char));

  field = strstr (content_buffer, pattern);
  if (!field) {
    return -1;
  }
  subpart = field + FIELD_SIZE;

  /* Avoid memory overlapping - this can be enhanced anyway */
  memcpy (incipit, content_buffer, field - content_buffer);
  memcpy (excipit, subpart, &content_buffer[FILE_SIZE - 1] - subpart);

  memset (content_buffer, 0, FILE_SIZE);

  /* Complete the file */
  strcat (content_buffer, incipit);
  strcat (content_buffer, val);
  strcat (content_buffer, excipit);

  free (incipit);
  free (excipit);

  return 0;
}

static int
load_template (char *content_buffer, const char *filename)
{
  FILE *template_file = NULL;
  int err = 0;

  template_file = fopen (filename, "r");
  fprintf (stdout, "Loading file: %s\n", filename);
  if (!template_file) {
    fprintf (stderr, "Error while loading the template\n");
    return -1;
  }

  err = fread ((void *) content_buffer, FILE_SIZE, 1, template_file);
  if (err < 0) {
    fprintf (stderr, "Cannot read template\n");
    err = -1;
    goto close;
  }

close:
  fclose (template_file);
  return err;
}

int
write_result (const char *filename, const char *content_buffer)
{
  FILE *output_file = NULL;
  int err = 0;

  output_file = fopen (filename, "w");
  if (!output_file) {
    fprintf (stderr, "Error while opening the file\n");
    return -1;
  }

  err = (int) fwrite (content_buffer, strlen (content_buffer), 1, output_file);
  fclose (output_file);

  return err > 0 ? 0 : -1;
}

/*
 * It should edit: UN_RESULT, MU_RESULT, FI_RESULT
 * They have FIELD_SIZE chars each
 */
int
result_rm_test (const float mu, const float u)
{
  char content_buffer[FILE_SIZE] = { 0 };
  int err = 0;

  err |= load_template (content_buffer, "docs/rm_test.tex.in");
  if (err)
    return err;

  err |= replace_val (content_buffer, "__UN_RESULT__", u);
  err |= replace_val (content_buffer, "__MU_RESULT__", mu);
  err |=
      replace_str (content_buffer, "_SCHED_RESULT_",
      mu <= u ? "schedulable" : "unschedulable");
  if (err)
    return err;
  err |= write_result ("docs/rm_test.tex", content_buffer);

  return err;
}

/*
 * It should edit: MU_RESULT, FI_RESULT
 * They have FIELD_SIZE chars each
 */
int
result_edf_test (const float mu)
{
  char content_buffer[FILE_SIZE] = { 0 };
  int err = 0;

  err |= load_template (content_buffer, "docs/edf_test.tex.in");
  if (err)
    return err;

  err |= replace_val (content_buffer, "__MU_RESULT__", mu);
  err |=
      replace_str (content_buffer, "_SCHED_RESULT_",
      mu <= 1 ? "schedulable" : "unschedulable");
  if (err)
    return err;

  err |= write_result ("docs/edf_test.tex", content_buffer);

  return err;
}

/*
 * It should edit: MU_RESULT, FI_RESULT
 * They have FIELD_SIZE chars each
 */
int
result_llf_test (const float mu)
{
  char content_buffer[FILE_SIZE] = { 0 };
  int err = 0;

  err |= load_template (content_buffer, "docs/llf_test.tex.in");
  if (err)
    return err;

  err |= replace_val (content_buffer, "__MU_RESULT__", mu);
  err |=
      replace_str (content_buffer, "_SCHED_RESULT_",
      mu <= 1 ? "schedulable" : "unschedulable");
  if (err)
    return err;

  err |= write_result ("docs/llf_test.tex", content_buffer);

  return err;
}

int
enable_test_slide (const int flags, const char *filename)
{
  char content_buffer[FILE_SIZE] = { 0 };
  char filename_full[100] = "";
  int err = 0;

  strcat (filename_full, filename);
  err |= load_template (content_buffer, strcat (filename_full, ".in"));
  if (err) {
    printf ("Cannot load the template\n");
    return err;
  }

  err |=
      replace_str (content_buffer, "__ENABLE_RMT__",
      (flags & ENABLE_RMT) ? "" : "%");
  err |=
      replace_str (content_buffer, "__ENABLE_EDF__",
      (flags & ENABLE_EDF) ? "" : "%");
  err |=
      replace_str (content_buffer, "__ENABLE_LLF__",
      (flags & ENABLE_LLF) ? "" : "%");
  err |=
      replace_str (content_buffer, "__ENABLE_WRP__",
      (flags & ENABLE_WRP) ? "" : "%");
  if (err) {
    printf ("Cannot find a pattern\n");
    return err;
  }

  err |= write_result (filename, content_buffer);

  return err;
}
