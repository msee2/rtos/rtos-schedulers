/*
  Instituto Tecnologico de Costa Rica
  Master in Electronics Engineering
  RTOS - I-2021
  Author:
    - Luis G Leon Vega <lleon95@gmail.com>
*/

#include "latex_engine.h"

#include <stdlib.h>

int
make_presentation ()
{
  int res = 0;
  res |= system ("make clean_pdf &> /dev/null && make docs &> /dev/null");
  return res;
}
