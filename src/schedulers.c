/*
  Instituto Tecnologico de Costa Rica
  Master in Electronics Engineering
  RTOS - I-2021
  Author:
    - Jean P. Jimenez
    - Luis G Leon Vega <lleon95@gmail.com>
*/

#include "schedulers.h"

/*----------------- Printers -----------------*/

void
print_tasks (TaskConfig * tasks, gint n_tasks)
{
  gint i = 0;
  g_print
      ("Index\t Task ID\t Computation Time\t Period\t Deadline\t Laxity\t status\n");
  for (i = 0; i < n_tasks; i++) {
    g_print ("%d\t %d\t\t %d\t\t\t %d\t %d\t\t %d\t %d\n", i, tasks[i].id,
        tasks[i].computation_time, tasks[i].period, tasks[i].deadline,
        tasks[i].laxity, tasks[i].state);
  }
  g_print ("\n");
}

void
print_sim (Simulation * sim, gint sim_time)
{
  gint i = 0;

  g_print ("\nProccess Scheduling\n");
  g_print ("Time\t Process Running\n");
  for (i = 0; i < sim_time; i++) {
    g_print ("%d-%d\t %d\n", i, i + 1, sim->ids[i]);
  }

  g_print ("\nProcess Missed Deadline Report\n");
  g_print ("Time\t Process\n");
  for (i = 0; i < sim->count; i++) {
    g_print ("%d-%d\t %d\n", sim->missed_deadlines[i],
        sim->missed_deadlines[i] + 1, sim->missed_ids[i]);
  }
  g_print ("\n");
}

/*----------------- Helpers -----------------*/
gint
gcd (gint a, gint b)
{
  if (a == 0)
    return b;
  else
    return gcd (b % a, a);
}

gint
lcm (TaskConfig * tasks, gint n)
{
  gint mcm, i = 1;

  mcm = tasks[0].period;
  for (i = 1; i < n; i++) {
    mcm = (mcm / gcd (mcm, tasks[i].period)) * tasks[i].period;
  }

  return mcm;
}

/*----------------- Checkers -----------------*/

gdouble
compute_utilisation (TaskConfig * tasks, gint n_tasks)
{
  gdouble utilization = 0.;
  gint i = 0;

  g_return_val_if_fail (tasks, 0.);
  g_return_val_if_fail (n_tasks >= 0, 0.);

  for (i = 0; i < n_tasks; i++) {
    utilization +=
        (gdouble) tasks[i].computation_time / (gdouble) tasks[i].period;
  }

  return utilization;
}

gdouble
compute_max_utilisation (TaskConfig * tasks, gint n_tasks)
{
  gdouble exponent = 0.;

  g_return_val_if_fail (tasks, 0.);
  g_return_val_if_fail (n_tasks >= 0, 0.);

  exponent = 1 / (gdouble) n_tasks;
  return ((gdouble) n_tasks * (pow (2, exponent) - 1.0));
}

gboolean
rm_check_lub (TaskConfig * tasks, gint n_tasks)
{
  gdouble utilization = 0., max_utilization = 0.69;

  g_return_val_if_fail (tasks, FALSE);
  g_return_val_if_fail (n_tasks >= 0, FALSE);

  max_utilization = compute_max_utilisation (tasks, n_tasks);

  utilization = compute_utilisation (tasks, n_tasks);

#ifdef DEBUG
  g_print ("Ut = %lf\t U = %lf\n", max_utilization, utilization);
#endif

  if (utilization <= max_utilization)
    return TRUE;
  else
    return FALSE;
}

gboolean
rm_check_hb (TaskConfig * tasks, gint n_tasks)
{
  gdouble utilization = 1;

  g_return_val_if_fail (tasks, FALSE);
  g_return_val_if_fail (n_tasks >= 0, FALSE);

  utilization = compute_utilisation (tasks, n_tasks);

#ifdef DEBUG
  g_print ("Ut = %lf\t U = %lf\n", 2., utilization);
#endif
  if (utilization <= 2.0)
    return TRUE;
  else
    return FALSE;
}

gboolean
edf_check (TaskConfig * tasks, gint n_tasks)
{
  gdouble utilization = 0.;

  g_return_val_if_fail (tasks, FALSE);
  g_return_val_if_fail (n_tasks >= 0, FALSE);

  utilization = compute_utilisation (tasks, n_tasks);

#ifdef DEBUG
  g_print ("Ut = %lf\t U = %lf\n", 1., utilization);
#endif
  if (utilization <= 1.)
    return TRUE;
  else
    return FALSE;
}

/*----------------- Task Helpers -----------------*/

/**
 * Initialises the tasks before execution
 * @param tasks task array
 * @param n_tasks number of tasks
 */
inline static void
tasks_init (TaskConfig * tasks, int n_tasks)
{
  gint i = 0;

  g_return_if_fail (tasks);
  g_return_if_fail (n_tasks >= 0);

  for (i = 0; i < n_tasks; i++) {
    tasks[i].remaining_time = tasks[i].computation_time;
    tasks[i].deadline = tasks[i].period;
    tasks[i].laxity = tasks[i].deadline - tasks[i].remaining_time;
    tasks[i].state = READY;
  }
}

/**
 * Sorts tasks by priority before execution
 * @param tasks task array
 * @param n_tasks number of tasks
 */
inline static void
sort_priority (TaskConfig * tasks, gint n_tasks)
{
  TaskConfig temp = { 0 };
  gint i = 0, j = 0;

  g_return_if_fail (tasks);
  g_return_if_fail (n_tasks >= 0);

  /* We can do a better job here with another technique. However, it
     leads to early optimisation and the course is not about it */
  for (i = 0; i < n_tasks; i++) {
    for (j = i; j < n_tasks; j++) {
      if (tasks[j].period < tasks[i].period) {
        temp = tasks[i];
        tasks[i] = tasks[j];
        tasks[j] = temp;
      }
    }
  }
}

/*----------------- Simulation Helpers -----------------*/

/**
 * Update the status of a task given a point in the simulation
 * @param tasks task array
 * @param n_tasks number of tasks within the array
 * @param sim simulation object
 * @param runtime instant in the simulation time
 */
inline static void
update_status (TaskConfig * tasks, gint n_tasks, Simulation * sim, gint runtime)
{
  gint i = 0;

  g_return_if_fail (tasks);
  g_return_if_fail (sim);
  g_return_if_fail (n_tasks >= 0);

  /* We can do a better job here with another technique. However, it
     leads to early optimisation and the course is not about it */
  for (i = 0; i < n_tasks; i++) {
    tasks[i].laxity = tasks[i].deadline - tasks[i].remaining_time - runtime;

    if ((tasks[i].state == FINISHED) && ((runtime + 1) % tasks[i].period == 0)) {
      tasks[i].remaining_time = tasks[i].computation_time;
      tasks[i].state = READY;
    }

    /* Record if there is any miss deadline */
    else if ((runtime + 1) % tasks[i].deadline == 0) {
      sim->missed_ids[sim->count] = tasks[i].id;
      sim->missed_deadlines[sim->count] = runtime + 1;
      sim->count += 1;
      /* Comment the lines below to disable a task due to deadline miss */
      tasks[i].deadline += tasks[i].period;
      tasks[i].state = FINISHED;
    }
  }
}

/**
 * RM next task selector
 * @param tasks task array
 * @param n_tasks number of tasks within the array
 */
inline static gint
rm_next_task (TaskConfig * tasks, gint n_tasks)
{
  gint i = 0;

  g_return_val_if_fail (tasks, -1);
  g_return_val_if_fail (n_tasks >= 0, -1);

  for (i = 0; i < n_tasks; i++) {
    if (tasks[i].state == READY) {
      return i;
    }
  }
  return -1;                    /* No task ready */
}

/**
 * EDF next task selector
 * @param tasks task array
 * @param n_tasks number of tasks within the array
 */
inline static gint
edf_next_task (TaskConfig * tasks, gint n_tasks)
{
  gint i = 0, earliest_deadline = n_tasks - 1;
  gboolean flag = FALSE;

  g_return_val_if_fail (tasks, -1);
  g_return_val_if_fail (n_tasks >= 0, -1);

  for (i = n_tasks - 1; i >= 0; i--) {
    /* When there is only 1 task ready the first if select it */
    if ((tasks[i].state == READY)
        && (tasks[i].deadline <= tasks[earliest_deadline].deadline)) {
      earliest_deadline = i;
      flag = TRUE;
    }
  }

  if (flag) {
    return earliest_deadline;
  } else {
    return -1;
  }
}

/**
 * LLF next task selector
 * @param tasks task array
 * @param n_tasks number of tasks within the array
 */
inline static gint
llf_next_task (TaskConfig * tasks, gint n_tasks)
{
  gint i = 0, least_laxity = n_tasks - 1;
  gboolean flag = FALSE;

  g_return_val_if_fail (tasks, -1);
  g_return_val_if_fail (n_tasks >= 0, -1);


  for (i = n_tasks - 1; i >= 0; i--) {
    /* When there is only 1 task ready the first if select it */
    if ((tasks[i].state == READY)
        && (tasks[i].laxity <= tasks[least_laxity].laxity)) {
      least_laxity = i;
      flag = TRUE;
    }
  }

  if (flag) {
    return least_laxity;
  } else {
    return -1;
  }
}

/*----------------- Simulation runners -----------------*/

void
run_rm_simulation (TaskConfig * tasks, gint n_tasks, Simulation * sim,
    gint simulation_time)
{
  gint runtime = 0;
  gint current = 0;

  g_return_if_fail (tasks);
  g_return_if_fail (sim);
  g_return_if_fail (simulation_time > 0);
  g_return_if_fail (n_tasks >= 0);

  tasks_init (tasks, n_tasks);
  sort_priority (tasks, n_tasks);

  sim->count = 0;

  for (runtime = 0; runtime < simulation_time; runtime++) {
    sim->ids[runtime] = tasks[current].id;
    --tasks[current].remaining_time;

    /* Check if the task finished and update all tasks states */
    if (tasks[current].remaining_time == 0) {
      tasks[current].state = FINISHED;
      tasks[current].deadline += tasks[current].period;
    }

    update_status (tasks, n_tasks, sim, runtime);

    /* Compute next task to run (can be the same or a preemptive task) */
    current = rm_next_task (tasks, n_tasks);

    /* Check if there is no task ready */
    while (current == -1) {
      ++runtime;
      sim->ids[runtime] = NOTASK;
      update_status (tasks, n_tasks, sim, runtime);
      current = rm_next_task (tasks, n_tasks);
    }
  }
}

void
run_edf_simulation (TaskConfig * tasks, gint n_tasks, Simulation * sim,
    gint simulation_time)
{
  gint runtime = 0;
  gint current = 0;

  g_return_if_fail (tasks);
  g_return_if_fail (sim);
  g_return_if_fail (simulation_time > 0);
  g_return_if_fail (n_tasks >= 0);

  tasks_init (tasks, n_tasks);
  sort_priority (tasks, n_tasks);

  current = edf_next_task (tasks, n_tasks);
  sim->count = 0;

  for (runtime = 0; runtime < simulation_time; runtime++) {
    sim->ids[runtime] = tasks[current].id;

    --tasks[current].remaining_time;
    if (tasks[current].remaining_time == 0) {
      tasks[current].state = FINISHED;
      tasks[current].deadline += tasks[current].period;
    }

    update_status (tasks, n_tasks, sim, runtime);

    /* 9 step compute next task to run (can be the same or a preemptive task) */
    current = edf_next_task (tasks, n_tasks);

    /* Check if there is no task ready */
    while (current == -1) {
      ++runtime;
      sim->ids[runtime] = NOTASK;
      update_status (tasks, n_tasks, sim, runtime);
      current = edf_next_task (tasks, n_tasks);
    }
  }
}

void
run_llf_simulation (TaskConfig * tasks, gint n_tasks, Simulation * sim,
    gint simulation_time)
{
  gint runtime = 0;
  gint current = 0;

  g_return_if_fail (tasks);
  g_return_if_fail (sim);
  g_return_if_fail (simulation_time > 0);
  g_return_if_fail (n_tasks >= 0);

  tasks_init (tasks, n_tasks);
  sort_priority (tasks, n_tasks);

  current = llf_next_task (tasks, n_tasks);
  sim->count = 0;

  for (runtime = 0; runtime < simulation_time; runtime++) {
    sim->ids[runtime] = tasks[current].id;

    --tasks[current].remaining_time;
    if (tasks[current].remaining_time == 0) {
      tasks[current].state = FINISHED;
      tasks[current].deadline += tasks[current].period;
    }

    update_status (tasks, n_tasks, sim, runtime);

    /* 9 step compute next task to run (can be the same or a preemptive task) */
    current = llf_next_task (tasks, n_tasks);

    /* Check if there is no task ready */
    while (current == -1) {
      ++runtime;
      sim->ids[runtime] = NOTASK;
      update_status (tasks, n_tasks, sim, runtime);
      current = llf_next_task (tasks, n_tasks);
    }
  }
}
