/*
  Instituto Tecnologico de Costa Rica
  Master in Electronics Engineering
  RTOS - I-2021
  Author:
    - Luis G Leon Vega <lleon95@gmail.com>
*/

#include "runner.h"

#include "configs.h"
#include "gui.h"
#include "latex_engine.h"
#include "schedulers.h"

inline static gboolean
check_tasks (Gui * gui)
{
  AnalysisConfig *analysis_config;
  TaskConfig *task_config;
  gboolean valid = TRUE;
  GtkWidget *dialog;
  GtkDialogFlags flags = GTK_DIALOG_DESTROY_WITH_PARENT;
  gint i = 0;

  g_return_val_if_fail (gui, FALSE);

  analysis_config = &(gui->analysis);

  /* Get the task info for every defined task */
  for (i = 0; i < gui->n_tasks; ++i) {
    /* Get task info */
    task_config = &(analysis_config->configs[i]);
    set_task_config (gui, task_config, i);
    valid = validate_entry (task_config);
    if (!valid) {
      dialog = gtk_message_dialog_new (gui->window,
          flags,
          GTK_MESSAGE_ERROR,
          GTK_BUTTONS_CLOSE,
          "Error while defining task '%i'. Please check Pi and Ci", i + 1);
      gtk_dialog_run (GTK_DIALOG (dialog));
      gtk_widget_destroy (dialog);
      return FALSE;
    }
  }
  return TRUE;
}

inline static void
uninitialise_simulation_object (Simulation * sim)
{
  g_return_if_fail (sim);

  /* Free memory if any - the element must be zeroed */
  g_free (sim->ids);
  g_free (sim->missed_deadlines);
  g_free (sim->missed_ids);
  g_free (sim->periods);

  sim->ids = NULL;
  sim->missed_deadlines = NULL;
  sim->missed_ids = NULL;
  sim->periods = NULL;
}

inline static void
initialise_simulation_object (Gui * gui, Simulation * sim)
{
  gsize runtime = 0;

  g_return_if_fail (sim);
  g_return_if_fail (gui);

  runtime = (gsize) gui->analysis.minimum_multiplier;

  /* Free memory if any - the element must be zeroed */
  uninitialise_simulation_object (sim);

  /* Reserve memory for the simulation */
  sim->ids = g_new0 (gint, runtime);
  sim->missed_deadlines = g_new0 (gint, runtime);
  sim->missed_ids = g_new0 (gint, runtime);
  sim->periods = g_new0 (gint, runtime);
  sim->tasks = gui->analysis.configs;
}


void
on_start_execution (GtkWidget * widget, gpointer * data)
{
  Gui *gui = NULL;
  AnalysisConfig *analysis_config = NULL;
  Simulation *sim = NULL;
  Simulation rm_sim = { 0 }, edf_sim = { 0 }, llf_sim = { 0 };
  TaskConfig *task_config = NULL;
  guint n_tasks = 0;
  guint err = 0, local_err = 0;
  gint flags = 0;
  TaskConfig rm_tasks[MAX_TASKS] = { 0 };
  TaskConfig edf_tasks[MAX_TASKS] = { 0 };
  TaskConfig llf_tasks[MAX_TASKS] = { 0 };

  GtkWidget *button;

  g_return_if_fail (data);
  gui = (Gui *) (data);

  analysis_config = &(gui->analysis);
  sim = &(gui->simulation);
  task_config = analysis_config->configs;
  n_tasks = gui->n_tasks;

  memcpy((void *) rm_tasks, (void *) task_config, MAX_TASKS * sizeof(TaskConfig));
  memcpy((void *) edf_tasks, (void *) task_config, MAX_TASKS * sizeof(TaskConfig));
  memcpy((void *) llf_tasks, (void *) task_config, MAX_TASKS * sizeof(TaskConfig));

  button = GTK_WIDGET (gtk_builder_get_object (gui->builder, "start-btn"));
  gtk_widget_set_sensitive (button, FALSE);

  /* Get the task info for every defined task */
  if (!check_tasks (gui)) {
    goto enable_button;
  }

  initialise_simulation_object (gui, sim);
  create_tasks_frame (gui, n_tasks, FALSE, "docs/tasks.tex");

  /* First, reexecute the schedulability test. It will refresh the settings */
  on_check_algorithms (widget, data);

  /* Run the simulation where it applies */
  if (analysis_config->rm_scheduler) {
#ifdef PRINT_SIMS_STDOUT
    g_print ("--------------------------\n");
    g_print ("Simulating: Rate Monotonic\n");
    g_print ("--------------------------\n");
#endif
    initialise_simulation_object (gui, &rm_sim);
    run_rm_simulation (rm_tasks, n_tasks, &rm_sim,
        analysis_config->minimum_multiplier);
    local_err =
        result_rm_test (analysis_config->processing_load, analysis_config->u_n);
    err |= local_err;
    if (local_err) {
      g_printerr ("Cannot place the result RM test in LaTex\n");
    }
    create_execution_frame (&rm_sim, n_tasks,
        analysis_config->minimum_multiplier, "docs/rm_execution_results.tex");
    analysis_config->sim_rm_schedulable = (rm_sim.count == 0);
#ifdef PRINT_SIMS_STDOUT
    print_sim (&rm_sim, analysis_config->minimum_multiplier);
#endif
  }
  if (analysis_config->edf_scheduler) {
#ifdef PRINT_SIMS_STDOUT
    g_print ("-----------------------------------\n");
    g_print ("Simulating: Earliest Deadline First\n");
    g_print ("-----------------------------------\n");
#endif
    initialise_simulation_object (gui, &edf_sim);
    run_edf_simulation (edf_tasks, n_tasks, &edf_sim,
        analysis_config->minimum_multiplier);
    local_err = result_edf_test (analysis_config->processing_load);
    err |= local_err;
    if (local_err) {
      g_printerr ("Cannot place the result EDF test in LaTex\n");
    }
    create_execution_frame (&edf_sim, n_tasks,
        analysis_config->minimum_multiplier, "docs/edf_execution_results.tex");
    analysis_config->sim_edf_schedulable = (edf_sim.count == 0);
#ifdef PRINT_SIMS_STDOUT
    print_sim (&edf_sim, analysis_config->minimum_multiplier);
#endif
  }
  if (analysis_config->llf_scheduler) {
#ifdef PRINT_SIMS_STDOUT
    g_print ("------------------------------\n");
    g_print ("Simulating: Least Laxity First\n");
    g_print ("------------------------------\n");
#endif
    initialise_simulation_object (gui, &llf_sim);
    run_llf_simulation (llf_tasks, n_tasks, &llf_sim,
        analysis_config->minimum_multiplier);
    local_err = result_llf_test (analysis_config->processing_load);
    err |= local_err;
    if (local_err) {
      g_printerr ("Cannot place the result LLF test in LaTex\n");
    }
    create_execution_frame (&llf_sim, n_tasks,
        analysis_config->minimum_multiplier, "docs/llf_execution_results.tex");
    analysis_config->sim_llf_schedulable = (llf_sim.count == 0);
#ifdef PRINT_SIMS_STDOUT
    print_sim (&llf_sim, analysis_config->minimum_multiplier);
#endif
  }

  flags |= analysis_config->rm_scheduler ? ENABLE_RMT : 0;
  flags |= analysis_config->edf_scheduler ? ENABLE_EDF : 0;
  flags |= analysis_config->llf_scheduler ? ENABLE_LLF : 0;

  local_err |= enable_test_slide (flags, "docs/schedulability.tex");
  if (!analysis_config->show_per_slide) {
    local_err |= enable_test_slide (flags, "docs/execution.tex");
  } else {
    create_wrap_execution_frame (analysis_config, &rm_sim, &edf_sim, &llf_sim,
        n_tasks, analysis_config->minimum_multiplier,
        "docs/wrap_execution.tex");
    local_err |= enable_test_slide (ENABLE_WRP, "docs/execution.tex");
  }
  local_err |= enable_test_slide (flags, "docs/description.tex");
  err |= local_err;
  if (local_err) {
    g_printerr ("Cannot enable the slides\n");
  }

  if (err) {
    g_printerr ("There was a failure while generating the schedulability"
        " tests\n");
  }

  /* Create the tasks frame */
  create_tasks_frame (gui, n_tasks, TRUE, "docs/tasks_results.tex");

  /* Create the presentation */
  make_presentation ();

enable_button:
  gtk_widget_set_sensitive (button, TRUE);
  uninitialise_simulation_object (&llf_sim);
  uninitialise_simulation_object (&edf_sim);
  uninitialise_simulation_object (&rm_sim);
  uninitialise_simulation_object (sim);
}

void
on_check_algorithms (GtkWidget * widget, gpointer * data)
{
  Gui *gui = NULL;
  AnalysisConfig *analysis_config;
  TaskConfig *task_config;
  GtkWidget *label;
  gchar *val_str = NULL;

  g_return_if_fail (data);
  gui = (Gui *) (data);

  analysis_config = &(gui->analysis);
  task_config = analysis_config->configs;

  /* Get scheduler selection */
  get_selected_options (gui);
  if (!check_tasks (gui)) {
    goto invalid_settings;
  }
  g_print ("Checking schedulability ...\n");

  /* Check schedulability */
  if (analysis_config->rm_scheduler) {
    analysis_config->rm_schedulable = rm_check (task_config, gui->n_tasks);
  }
  if (analysis_config->edf_scheduler) {
    analysis_config->edf_schedulable = edf_check (task_config, gui->n_tasks);
  }
  if (analysis_config->llf_scheduler) {
    /* It is the same */
    analysis_config->llf_schedulable = edf_check (task_config, gui->n_tasks);
  }

  /* Show common values */
  analysis_config->minimum_multiplier = lcm (task_config, gui->n_tasks);
  analysis_config->u_n = compute_max_utilisation (task_config, gui->n_tasks);
  analysis_config->processing_load =
      compute_utilisation (task_config, gui->n_tasks);
  show_common_values (gui);

  /* Compute values */
  label = GTK_WIDGET (gtk_builder_get_object (gui->builder, "schd-lbl"));
  val_str = g_strdup_printf ("%s%s%s",
      analysis_config->rm_scheduler ? "RM" : "",
      analysis_config->edf_scheduler ? " EDF" : "",
      analysis_config->llf_scheduler ? " LLF" : "");
  gtk_label_set_text (GTK_LABEL (label), val_str);
  g_print ("Selected schedulers: %s\n", val_str);
  g_free (val_str);

  label = GTK_WIDGET (gtk_builder_get_object (gui->builder, "sch-lbl"));
  val_str = g_strdup_printf ("%s%s%s",
      analysis_config->rm_schedulable ? "RM" : "",
      analysis_config->edf_schedulable ? " EDF" : "",
      analysis_config->llf_schedulable ? " LLF" : "");
  gtk_label_set_text (GTK_LABEL (label), val_str);

  g_print ("It is schedulable in: %s\n", val_str);
  g_free (val_str);
  return;

invalid_settings:
  g_print ("Invalid settings. Please, check the tasks\n");
}
